import { Component, OnInit } from '@angular/core';
import { PackageApi } from '../../api/package.api';
import { Package } from '../../model/package.model';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { TableAction } from 'src/app/models/table-action.model';
import { Mutation } from './../../../../models/mutation.const';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.css']
})
export class PackagesComponent implements OnInit {

  public originPackages: Package[];
  public copyPackages: Package[];

  public originPackagesLoading: boolean = true;
  public copyPackagesLoading: boolean = true;

  public originActions: TableAction[] = [
    {
      tooltip: 'Kopiraj',
      icon: 'copy',
      command: (row: any) => {
        this.createPackage(row as Package, Mutation.CREATE);
      }
    }
  ];
  public copyActions: TableAction[] = [
    {
      tooltip: 'Uredi',
      icon: 'pencil',
      command: (row: any) => {
        this.createPackage(row as Package, Mutation.UPDATE);
      }
    },
    {
      tooltip: 'Obriši',
      icon: 'trash',
      command: (row: any) => {
        this.deletePackage(row as Package);
      }
    }
  ];

  constructor(private packageApi: PackageApi,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.fetchPackages();
  }

  private fetchPackages(): void {
    this.originPackagesLoading = true;
    this.copyPackagesLoading = true;

    this.packageApi.getAllPackagesOrigin()
      .pipe(
        catchError(() => {
          return of([]);
        }),
        tap(packages => this.originPackages = packages)
      )
      .subscribe();

    this.packageApi.getAllPackagesCopy()
    .pipe(
      catchError(() => {
        return of([]);
      }),
      tap(packages => this.copyPackages = packages)
    )
    .subscribe();
  } 

  public createPackage(packageItem: Package, mutation: Mutation): void {
    this.router.navigate(['create'], {relativeTo: this.route, queryParams: { ...packageItem, mutation }});
  }

  public deletePackage(packageItem: Package): void {
    this.packageApi.deletePackageCopy(packageItem)
      .pipe(tap())
      .subscribe(res => this.fetchPackages());
  }
}
