import { Component, OnInit } from '@angular/core';
import { PackageApi } from '../../api/package.api';
import { Package } from '../../model/package.model';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Mutation } from 'src/app/models/mutation.const';

@Component({
  selector: 'app-create-package',
  templateUrl: './create-package.component.html',
  styleUrls: ['./create-package.component.css']
})
export class CreatePackageComponent implements OnInit {

  public mutation: Mutation = Mutation.CREATE;
  public package: Package;

  constructor(private packageApi: PackageApi,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        tap(packageParam => {
          this.package = Object.assign({}, packageParam) as Package;
          this.mutation = packageParam['mutation'];
        })
      )
      .subscribe();
  }

  public save(newPackage: Package) {
    if (this.mutation === Mutation.CREATE) {
      this.create(newPackage);
    } else {
      this.update(this.package, newPackage);
    }
  }

  public create(newPackage: Package) {
    this.packageApi.createPackageCopy(newPackage)
      .pipe(tap())
      .subscribe(res => this.router.navigate(['../'], { relativeTo: this.route }));
  }

  public update(oldPackage: Package, newPackage: Package) {
    this.packageApi.updatePackageCopy(oldPackage, newPackage)
      .pipe(tap())
      .subscribe(res => this.router.navigate(['../'], { relativeTo: this.route }));
  }

  public cancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
