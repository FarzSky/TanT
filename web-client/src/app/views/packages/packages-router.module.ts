import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PackagesComponent } from './pages/packages/packages.component';
import { CreatePackageComponent } from './pages/create-package/create-package.component';

const routes: Routes = [
    {
      path: '',
      component: PackagesComponent,
    },
    {
      path: 'create',
      component: CreatePackageComponent
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class PackagesRouterModule { }