import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { Package } from '../../model/package.model';
import { TableColumn } from 'src/app/models/table-column.model';
import { TableAction } from 'src/app/models/table-action.model';

@Component({
  selector: 'app-packages-table',
  templateUrl: './packages-table.component.html',
  styleUrls: ['./packages-table.component.scss']
})
export class PackagesTableComponent implements OnInit, OnChanges {

    constructor() { }

    public columns: TableColumn[] = [];
    @Input() packages: Package[] = [];
    @Input() packageActions: TableAction[] = [];

    public loading: boolean = true;
    @Input() 
    get isLoading() { return this.loading; }
    @Output() loadingChanged: EventEmitter<boolean> = new EventEmitter();
    set isLoading(isloading: boolean) {
      this.loading = isloading;
      this.loadingChanged.emit(this.loading);
    }

    ngOnInit() {
      this.initTableColumns();
    }

    ngOnChanges(changes: SimpleChanges) {
      if (changes.packages) {
        this.loading = false;
      }
    }

    private initTableColumns() {
      this.columns = [
        { field: 'primatelj', header: 'Primatelj' },
        { field: 'adresa', header: 'Adresa' },
        { field: 'posta', header: 'Pošta' },
        { field: 'grad', header: 'Grad' },
        { field: 'telefon', header: 'Telefon' },
        { field: 'paket', header: 'Paket' },
        { field: 'cijena', header: 'Cijena' }
      ];
    }
}
