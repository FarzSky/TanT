import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Package } from '../../model/package.model';
import { FormGroup, FormControl } from '@angular/forms';
import get from 'lodash.get';

@Component({
  selector: 'app-package-form',
  templateUrl: './package-form.component.html',
  styleUrls: ['./package-form.component.css']
})
export class PackageFormComponent implements OnInit, OnChanges {

  @Input() package: Package;

  @Output() onSave: EventEmitter<Package> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter(); 

  public packageForm: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.buildPackageForm(this.package);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.package) {
      this.buildPackageForm(changes.package.currentValue);
    }
  }

  public buildPackageForm(form: Package): void {
    this.packageForm = new FormGroup({
      primatelj: new FormControl(get(form, 'primatelj', '')),
      adresa: new FormControl(get(form, 'adresa', '')),
      posta: new FormControl(get(form, 'posta', '')),
      grad: new FormControl(get(form, 'grad', '')),
      telefon: new FormControl(get(form, 'telefon', '')),
      paket: new FormControl(get(form, 'paket', '')),
      cijena: new FormControl(get(form, 'cijena', ''))
    });
  }

  public submit(): void {
    this.onSave.emit(this.packageForm.value);
  }

  public cancel(): void {
    this.onCancel.emit();
  }

}
