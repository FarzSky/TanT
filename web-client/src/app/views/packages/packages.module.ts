import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabViewModule } from 'primeng/tabview';

import { PackagesComponent } from './pages/packages/packages.component';
import { PackagesTableComponent } from './components/packages-table/packages-table.component';
import { PackagesRouterModule } from './packages-router.module';
import { PackageApi } from './api/package.api';
import { ComponentsModule } from './../../components/components.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PackageFormComponent } from './components/package-form/package-form.component';
import { CreatePackageComponent } from './pages/create-package/create-package.component';

import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  declarations: [
    PackagesComponent,
    PackagesTableComponent,
    PackageFormComponent,
    CreatePackageComponent
  ],
  imports: [
    PackagesRouterModule,
    CommonModule,
    TabViewModule,
    ComponentsModule,
    ReactiveFormsModule,
    ButtonModule,
    InputTextModule
  ],
  exports: [
    PackagesComponent,
    CreatePackageComponent
  ],
  providers: [
    PackageApi
  ]
})
export class PackagesModule { }
