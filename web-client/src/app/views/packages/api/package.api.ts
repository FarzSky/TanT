import { CoreService } from 'src/app/core/core.service';
import { Observable } from 'rxjs';
import { Package } from '../model/package.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class PackageApi {

    private apiOrigin = '/packages/origin';
    private apiCopy = '/packages/copy';

    constructor(
        private coreService: CoreService,
        private http: HttpClient
    ) { }

    public getAllPackagesOrigin(): Observable<Package[]> {
        return this.http.get<Package[]>(`${this.coreService.getApiHost()}${this.apiOrigin}`);
    }

    public getAllPackagesCopy(): Observable<Package[]> {
        return this.http.get<Package[]>(`${this.coreService.getApiHost()}${this.apiCopy}`);
    }

    public createPackageCopy(newPackage: Package): Observable<any> {
        return this.http.post(`${this.coreService.getApiHost()}${this.apiCopy}`, newPackage);
    }

    public updatePackageCopy(oldPackage: Package, newPackage: Package): Observable<any> {
        return this.http.put(`${this.coreService.getApiHost()}${this.apiCopy}`, { oldPackage, newPackage });
    }

    public deletePackageCopy(packageItem: Package): Observable<any> {
        return this.http.post(`${this.coreService.getApiHost()}${this.apiCopy}/delete`, packageItem);
    }
}