export interface Package {
    primatelj: string;
    adresa: string;
    posta: number;
    grad: string;
    telefon: string;
    paket: string;
    cijena: number;
}