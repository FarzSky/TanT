import { Component, OnInit, ViewChild, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { TableColumn } from './../../models/table-column.model';
import { Table } from 'primeng/table/table';
import { TableAction } from 'src/app/models/table-action.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges {

    constructor() { }

    @Input() columns: TableColumn[] = [];
    @Input() data: any[] = [];
    @Input() selected: any[] = [];
    @Input() tableActions: TableAction[] = [];

    loading: boolean = true;
    @Input() 
    get isLoading() { return this.loading; }
    @Output() loadingChanged: EventEmitter<boolean> = new EventEmitter();
    set isLoading(isloading: boolean) {
      this.loading = isloading;
      this.loadingChanged.emit(this.loading);
    }

    @ViewChild('dt') table: Table;

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
      if (changes.packages) {
        this.loading = false;
      }
    }

    public onActionItemClicked(row: any, action: Function): void {
      action(row);
    }
}
