import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToolbarModule } from 'primeng/toolbar';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { ButtonModule } from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';


import { TopBarComponent } from './top-bar/top-bar.component';
import { TableComponent } from './table/table.component';

@NgModule({
  declarations: [
    TopBarComponent,
    TableComponent
  ],
  imports: [
    CommonModule,
    ToolbarModule,
    TableModule,
    TooltipModule,
    ButtonModule,
    InputTextModule
  ],
  exports: [
    TopBarComponent,
    TableComponent
  ]
})
export class ComponentsModule { }
