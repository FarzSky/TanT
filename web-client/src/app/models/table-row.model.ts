import { SafeUrl } from '@angular/platform-browser';

export interface TableRow {
    [key: string]: string | number | SafeUrl;
}