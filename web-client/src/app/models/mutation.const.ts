export enum Mutation {
    CREATE = "create",
    UPDATE = "update",
    DELETE = "delete",
  }