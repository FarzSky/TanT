import { TableRow } from './table-row.model';

export interface TableAction {
    tooltip: string;
    icon: string;
    command(row: TableRow): void;
}