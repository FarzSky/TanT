import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
      path: 'packages',
      loadChildren: () => import('./views/packages/packages.module').then(m => m.PackagesModule)
    },
    { path: '',   redirectTo: '/packages', pathMatch: 'full' }, 
    { path: '**', redirectTo: '/packages', pathMatch: 'full' }
  ];
  
  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
  })
  export class AppRouterModule { }