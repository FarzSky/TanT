import { Optional, SkipSelf, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreService } from './core.service';

@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule
    ],
    providers: [
        CoreService
    ],
    bootstrap: []
  })
  export class CoreModule {
    constructor(
      @Optional()
      @SkipSelf()
      parentModule: CoreModule,
    ) {
      if (parentModule) {
        throw new Error('CoreModule is already loaded. Import only in AppModule');
      }
    }
  }