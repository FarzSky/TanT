package com.tiyanak.tant.views.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tiyanak.tant.core.usecases.UseCaseFactory
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC

class ProfileViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
            return ProfileViewModel(
                UseCaseFactory.createAuthUC(),
                UseCaseFactory.createPackageUC()
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}