package com.tiyanak.tant.views.signin

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.tiyanak.tant.R
import com.tiyanak.tant.common.afterTextChanged
import com.tiyanak.tant.data.Result
import com.tiyanak.tant.views.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class SignInActivity : AppCompatActivity() {

    private lateinit var signInViewModel: SignInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        signInViewModel = ViewModelProvider(this, SignInViewModelFactory())
            .get(SignInViewModel::class.java)

        setSignInListener()
        setFormListener()
        setViewModelStateListener()
        setViewModelSignInResultListener()
    }

    private fun setViewModelSignInResultListener() {
        // observe for sign in action attempt
        signInViewModel.signInResult.observe(this, Observer {
            val signInResult = it ?: return@Observer

            loading.visibility = View.GONE

            if (signInResult is Result.Error) {
                showSignInFailed(signInResult.errorCode)
            }

            setResult(Activity.RESULT_OK)
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        })
    }

    private fun setViewModelStateListener() {
        // observe for view state changes
        signInViewModel.signInDto.observe(this@SignInActivity, Observer {
            val signInState = it ?: return@Observer

            btn_sign_in.isEnabled = signInState.isDataValid

            if (signInState.usernameError != null) {
                username.error = getString(signInState.usernameError)
            }
            if (signInState.passwordError != null) {
                password.error = getString(signInState.passwordError)
            }
        })
    }

    private fun setFormListener() {
        // username validation
        username.afterTextChanged {
            signInViewModel.usernameChanged(it)
        }

        // password validation
        password.apply {
            afterTextChanged {
                signInViewModel.passwordChanged(it)
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        signInViewModel.signIn(username.text.toString(), password.text.toString())
                }
                false
            }
        }
    }

    private fun setSignInListener() {
        btn_sign_in.setOnClickListener {
            loading.visibility = View.VISIBLE
            signInViewModel.signIn(username.text.toString(), password.text.toString())
        }
    }

    private fun showSignInFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
}
