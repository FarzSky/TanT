package com.tiyanak.tant.core.usecases.interfaces

import com.tiyanak.tant.data.db.entities.SignedInUser
import com.tiyanak.tant.data.Result

interface IAuthUC {
    fun isSignedIn(): Boolean

    fun signOut()

    fun signIn(username: String, password: String): Result<SignedInUser>

    fun getUserName(): String
}