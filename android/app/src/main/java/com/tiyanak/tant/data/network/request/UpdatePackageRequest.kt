package com.tiyanak.tant.data.network.request

data class UpdatePackageRequest(
    val oldPackage: PackageRequest,
    val newPackage: PackageRequest
)