package com.tiyanak.tant.views.signin

data class SignedInDto(
    val username: String,
    val email: String
)
