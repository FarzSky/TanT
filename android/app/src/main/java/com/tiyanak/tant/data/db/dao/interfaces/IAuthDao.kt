package com.tiyanak.tant.data.db.dao.interfaces

import com.tiyanak.tant.data.db.entities.SignedInUser
import com.tiyanak.tant.data.Result

interface IAuthDao {
    fun signIn(username: String, password: String): Result<SignedInUser>

    fun signout()
}