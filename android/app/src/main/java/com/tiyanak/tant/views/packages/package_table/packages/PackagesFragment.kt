package com.tiyanak.tant.views.packages.package_table.packages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tiyanak.tant.R
import com.tiyanak.tant.components.adapters.PackageAdapter
import kotlinx.android.synthetic.main.fragment_packages.*

class PackagesFragment : Fragment() {

    private lateinit var viewModel: PackagesViewModel
    private lateinit var packagesAdapter: PackageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, PackagesViewModelFactory()).get(PackagesViewModel::class.java)
        packagesAdapter = PackageAdapter(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_packages, container, false)

        val rv = root.findViewById<RecyclerView>(R.id.rv_packages)
        rv.setHasFixedSize(true)
        rv.setItemViewCacheSize(20)
        rv.itemAnimator = DefaultItemAnimator()
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = packagesAdapter

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.mediator.observe(viewLifecycleOwner, Observer {  })
        viewModel.packages.observe(viewLifecycleOwner, Observer {
            packagesAdapter.setPackages(it)
        })

        packagesAdapter.packageActionEvent.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandledOrReturnNull()?.let {
                newState -> viewModel.updatePackageState(newState)
            }
        })
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): PackagesFragment {
            return PackagesFragment()
                .apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}