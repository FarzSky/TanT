package com.tiyanak.tant.components.view_holders

import android.view.View
import android.widget.TextView
import com.tiyanak.tant.R
import com.tiyanak.tant.views.packages.package_table.dto.PackageDto

class PackageViewHolder(itemView: View) : BaseViewHolder<PackageDto>(itemView) {

    override fun bind(item: PackageDto) {
        itemView.findViewById<TextView>(R.id.tv_receiver).text = item.primatelj
        itemView.findViewById<TextView>(R.id.tv_phone).text = item.telefon
        itemView.findViewById<TextView>(R.id.tv_location).text = item.lokacija
        itemView.findViewById<TextView>(R.id.tv_package).text = item.paket
        itemView.findViewById<TextView>(R.id.tv_price).text = item.cijena
        itemView.findViewById<TextView>(R.id.tv_state).text = item.state
    }
}