package com.tiyanak.tant.data.file_system

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import com.tiyanak.tant.R
import java.math.BigInteger
import java.security.*
import java.security.spec.AlgorithmParameterSpec
import java.security.spec.InvalidKeySpecException
import java.util.*
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.security.auth.x500.X500Principal

class EncryptedPreferences(context: Context) {

    private var encryptedStore: IEncryptedStore? = null

    init {

        encryptedStore = if (Build.VERSION.SDK_INT < 18) {
            EncryptedStore_SDK16()
        } else {
            EncryptedStore_SDK18()
        }

        var isInitialized = false

        try {
            isInitialized = encryptedStore!!.init(context)
        } finally {
            if (!isInitialized && encryptedStore is EncryptedStore_SDK18) {
                encryptedStore = EncryptedStore_SDK16()
                encryptedStore!!.init(context)
            }
        }

    }

    fun setData(key: String, data: ByteArray) {
        encryptedStore!!.setData(key, data)
    }

    fun getData(key: String): ByteArray? {
        return encryptedStore!!.getData(key)
    }

    fun remove(key: String) {
        encryptedStore!!.remove(key)
    }

    private interface IEncryptedStore {
        fun init(context: Context): Boolean

        fun setData(key: String, data: ByteArray?)

        fun getData(key: String): ByteArray?

        fun remove(key: String)
    }

    private class EncryptedStore_SDK16 : IEncryptedStore {

        private var preferences: SharedPreferences? = null

        override fun init(context: Context): Boolean {
            preferences = context.getSharedPreferences(
                context.getString(R.string.app_package) + "_preferences",
                Context.MODE_PRIVATE
            )
            return true
        }

        override fun setData(key: String, data: ByteArray?) {
            if (data == null)  return
            val editor = preferences!!.edit()
            editor.putString(key, Base64.encodeToString(data, Base64.DEFAULT))
            editor.apply()
        }

        override fun getData(key: String): ByteArray? {
            val res = preferences!!.getString(key, null) ?: return null
            return Base64.decode(res, Base64.DEFAULT)
        }

        override fun remove(key: String) {
            val editor = preferences!!.edit()
            editor.remove(key)
            editor.apply()
        }
    }

    private class EncryptedStore_SDK18 : IEncryptedStore {

        private var preferences: SharedPreferences? = null
        private var alias: String? = null

        @SuppressLint("NewApi")
        override fun init(context: Context): Boolean {
            preferences = context.getSharedPreferences(
                context.getString(R.string.app_package) + "_preferences",
                Context.MODE_PRIVATE
            )
            alias = context.getString(R.string.app_package)

            val ks: KeyStore?

            try {
                ks = KeyStore.getInstance(KEYSTORE_PROVIDER_ANDROID_KEYSTORE)

                //Use null to load Keystore with default parameters.
                ks!!.load(null)

                // Check if Private and Public already keys exists. If so we don't need to generate them again
                ks.getKey(alias, null) as PrivateKey
                if (ks.getCertificate(alias) != null) {
                    val publicKey = ks.getCertificate(alias).publicKey
                    if (publicKey != null) {
                        // All keys are available.
                        return true
                    }
                }
            } catch (ex: Exception) {
                return false
            }

            // Create a start and end time, for the validity range of the key pair that's about to be
            // generated.
            val start = GregorianCalendar()
            val end = GregorianCalendar()
            end.add(Calendar.YEAR, 10)

            // Specify the parameters object which will be passed to KeyPairGenerator
            val spec: AlgorithmParameterSpec
            if (Build.VERSION.SDK_INT < 23) {
                spec = KeyGenParameterSpec.Builder(alias!!, KeyProperties.PURPOSE_SIGN)
                    .setCertificateSubject(X500Principal("CN=" + alias!!))
                    .setCertificateSerialNumber(BigInteger.valueOf(5646))
                    .setKeyValidityStart(start.time)
                    .setKeyValidityEnd(end.time)
                    .setDigests(KeyProperties.DIGEST_SHA256)
                    .setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PSS)
                    .build()

            } else {
                spec = KeyGenParameterSpec.Builder(alias!!, KeyProperties.PURPOSE_DECRYPT)
                    .setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                    .build()
            }

            // Initialize a KeyPair generator using the the intended algorithm (in this example, RSA
            // and the KeyStore. This example uses the AndroidKeyStore.
            val kpGenerator: KeyPairGenerator
            try {
                kpGenerator = KeyPairGenerator.getInstance(
                    KEY_ALGORITHM_RSA,
                    KEYSTORE_PROVIDER_ANDROID_KEYSTORE
                )
                kpGenerator.initialize(spec)
                // Generate private/public keys
                kpGenerator.generateKeyPair()
            } catch (e: NoSuchAlgorithmException) {
                try {
                    ks.deleteEntry(alias)
                } catch (e1: Exception) {
                    // Just ignore any errors here
                }
            } catch (e: InvalidAlgorithmParameterException) {
                try {
                    ks.deleteEntry(alias)
                } catch (e1: Exception) {
                    // Just ignore any errors here
                }
            } catch (e: NoSuchProviderException) {
                try {
                    ks.deleteEntry(alias)
                } catch (e1: Exception) {
                    // Just ignore any errors here
                }
            }

            return true
        }

        override fun setData(key: String, data: ByteArray?) {
            var ks: KeyStore? = null
            try {
                ks = KeyStore.getInstance(KEYSTORE_PROVIDER_ANDROID_KEYSTORE)

                ks!!.load(null)
                if (ks.getCertificate(alias) == null) return

                val publicKey = ks.getCertificate(alias).publicKey ?: return

                val value = data?.let { encrypt(publicKey, it) }

                val editor = preferences!!.edit()
                editor.putString(key, value)
                editor.apply()
            } catch (e: java.lang.Exception) {
                try {
                    ks?.deleteEntry(alias)
                } catch (e1: Exception) {
                    // Just ignore any errors here
                }
            }

        }

        override fun getData(key: String): ByteArray? {
            var ks: KeyStore? = null
            try {
                ks = KeyStore.getInstance(KEYSTORE_PROVIDER_ANDROID_KEYSTORE)
                ks!!.load(null)
                val privateKey = ks.getKey(alias, null) as PrivateKey
                return decrypt(privateKey, preferences!!.getString(key, null))
            } catch (e: Exception) {
                try {
                    ks?.deleteEntry(alias)
                } catch (e1: Exception) {
                    // Just ignore any errors here
                }
            }

            return null
        }

        override fun remove(key: String) {
            val editor = preferences!!.edit()
            editor.remove(key)
            editor.apply()
        }

        companion object {

            private const val KEY_ALGORITHM_RSA = "RSA"

            private const val KEYSTORE_PROVIDER_ANDROID_KEYSTORE = "AndroidKeyStore"
            private const val RSA_ECB_PKCS1_PADDING = "RSA/ECB/PKCS1Padding"

            @SuppressLint("TrulyRandom")
            @Throws(
                NoSuchAlgorithmException::class,
                NoSuchPaddingException::class,
                InvalidKeyException::class,
                IllegalBlockSizeException::class,
                BadPaddingException::class,
                NoSuchProviderException::class,
                InvalidKeySpecException::class
            )
            private fun encrypt(encryptionKey: PublicKey, data: ByteArray): String {
                val cipher = Cipher.getInstance(RSA_ECB_PKCS1_PADDING)
                cipher.init(Cipher.ENCRYPT_MODE, encryptionKey)
                val encrypted = cipher.doFinal(data)
                return Base64.encodeToString(encrypted, Base64.DEFAULT)
            }

            @Throws(
                NoSuchAlgorithmException::class,
                NoSuchPaddingException::class,
                InvalidKeyException::class,
                IllegalBlockSizeException::class,
                BadPaddingException::class,
                NoSuchProviderException::class
            )

            private fun decrypt(decryptionKey: PrivateKey, encryptedData: String?): ByteArray? {
                if (encryptedData == null)
                    return null
                val encryptedBuffer = Base64.decode(encryptedData, Base64.DEFAULT)
                val cipher = Cipher.getInstance(RSA_ECB_PKCS1_PADDING)
                cipher.init(Cipher.DECRYPT_MODE, decryptionKey)
                return cipher.doFinal(encryptedBuffer)
            }
        }
    }

}