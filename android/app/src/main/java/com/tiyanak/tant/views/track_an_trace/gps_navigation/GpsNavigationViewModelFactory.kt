package com.tiyanak.tant.views.track_an_trace.gps_navigation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tiyanak.tant.core.usecases.UseCaseFactory
import java.lang.IllegalArgumentException

class GpsNavigationViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GpsNavigationViewModel::class.java)) {
            return GpsNavigationViewModel(
                UseCaseFactory.createPackageUC()
            ) as T
        }
        throw IllegalArgumentException("Unknown View Model");
    }

}