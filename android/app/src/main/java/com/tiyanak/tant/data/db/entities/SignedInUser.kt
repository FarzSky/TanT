package com.tiyanak.tant.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SignedInUser")
data class SignedInUser (
    @PrimaryKey val username: String,
    val email: String
)