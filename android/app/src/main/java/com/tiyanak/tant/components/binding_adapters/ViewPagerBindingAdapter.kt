package com.tiyanak.tant.components.binding_adapters

import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.viewpager.widget.ViewPager

@BindingAdapter("currentTab")
fun setCurrentTab(pager: ViewPager, itemLiveData: MutableLiveData<Int>){
    val currentTab = itemLiveData.value ?: 0
    itemLiveData.value?.let {
        if (pager.currentItem != currentTab) {
            pager.setCurrentItem(currentTab, true)
        }
    }
}
@InverseBindingAdapter(attribute = "currentTab", event = "android:currentTabAttrChanged")
fun getCurrentTab(pager: ViewPager) = MutableLiveData(pager.currentItem)
