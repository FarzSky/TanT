package com.tiyanak.tant.views.packages.package_table.packages_history

import androidx.lifecycle.*
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC
import com.tiyanak.tant.views.packages.package_table.dto.PackageDto

class PackagesHistoryViewModel(packageUC: IPackageUC) : ViewModel() {
    private val _packages: MutableLiveData<MutableList<PackageDto>> = MutableLiveData(mutableListOf())
    val packages: LiveData<MutableList<PackageDto>> = _packages

    val mediator = MediatorLiveData<Unit>()

    init {
        mediator.addSource(packageUC.getDeliveredPackages()) {
            _packages.value = it.toMutableList()
        }
    }
}