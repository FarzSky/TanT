package com.tiyanak.tant.common.converters

import androidx.room.TypeConverter
import java.text.SimpleDateFormat
import java.util.*

fun formateDate(date: Date?): String {
    if (date == null) return ""
    val pattern = "yyyy-MM-dd HH:mm"
    val simpleDateFormat = SimpleDateFormat(pattern, Locale.getDefault())
    return simpleDateFormat.format(date)
}

fun toStartOfMonth(date: Date): Date {
    val cal = Calendar.getInstance()
    cal.time = date
    cal.set(Calendar.DAY_OF_MONTH, 1)
    return cal.time
}

fun toEndOfMonth(date: Date): Date {
    val cal = Calendar.getInstance()
    cal.time = date
    cal.set(Calendar.DAY_OF_MONTH, cal.getMaximum(Calendar.DAY_OF_MONTH))
    return cal.time
}

fun toStartOfYear(date: Date): Date {
    val cal = Calendar.getInstance()
    cal.time = date
    cal.set(Calendar.DAY_OF_MONTH, 1)
    cal.set(Calendar.MONTH, 0)
    return cal.time
}

fun toEndOfYear(date: Date): Date{
    val cal = Calendar.getInstance()
    cal.time = date
    cal.set(Calendar.DAY_OF_MONTH, 31)
    cal.set(Calendar.MONTH, 11)
    return cal.time
}

class DateConverter {

    @TypeConverter
    fun toDate(timestamp: Long?): Date? {
        return if (timestamp == null) null else Date(timestamp)
    }

    @TypeConverter
    fun toTimestamp(date: Date?): Long? {
        return (date?.time)
    }

}