package com.tiyanak.tant.views.packages.package_table.packages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC
import com.tiyanak.tant.views.packages.package_table.dto.PackageDto
import com.tiyanak.tant.views.packages.package_table.dto.PackageUpdateStateDto

class PackagesViewModel(val packageUC: IPackageUC) : ViewModel() {

    private val _packages: MutableLiveData<MutableList<PackageDto>> = MutableLiveData(mutableListOf())
    val packages: LiveData<MutableList<PackageDto>> = _packages

    val mediator = MediatorLiveData<Unit>()

    init {
        mediator.addSource(packageUC.getInStoragePackages()) {
            _packages.value = it.toMutableList()
        }
    }

    fun updatePackageState(newPackageState: PackageUpdateStateDto) {
        packageUC.updatePackageState(newPackageState)
        _packages.postValue(
            _packages.value?.filter {
                it.adresa != newPackageState.address ||
                it.paket != newPackageState.`package` ||
                it.primatelj != newPackageState.receiver ||
                it.posta != newPackageState.post
            }?.toMutableList() ?: mutableListOf()
        )
    }

}