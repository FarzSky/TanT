package com.tiyanak.tant.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.tiyanak.tant.common.constants.IN_STORAGE
import java.util.*

@Entity(tableName = "Package")
data class Package(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    val primatelj: String,
    val adresa: String,
    val posta: Int,
    val grad: String,
    val telefon: String? = "",
    val paket: String? = "",
    val cijena: Double? = 0.0,
    val state: String = IN_STORAGE,
    val deliveredAt: Date? = null,
    val smsSent: Int = 0
)