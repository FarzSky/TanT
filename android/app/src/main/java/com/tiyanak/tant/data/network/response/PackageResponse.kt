package com.tiyanak.tant.data.network.response

data class PackageResponse(
    val primatelj: String,
    val adresa: String,
    val posta: Int,
    val grad: String,
    val telefon: String,
    val paket: String,
    val cijena: Double
)