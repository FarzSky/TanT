package com.tiyanak.tant.core.usecases.interfaces

import androidx.lifecycle.LiveData
import com.tiyanak.tant.views.packages.create_package.CreatePackageDto
import com.tiyanak.tant.views.packages.package_table.dto.PackageDto
import com.tiyanak.tant.views.packages.package_table.dto.PackageUpdateStateDto

interface IPackageUC {
    fun createPackage(createPackageDto: CreatePackageDto)
    fun updatePackageState(newPackageState: PackageUpdateStateDto)

    fun getInStoragePackages(): LiveData<List<PackageDto>>
    fun getDeliveredPackages(): LiveData<List<PackageDto>>
    fun getInDeliveryPackages(): LiveData<List<PackageDto>>

    fun countTodayDelivered(): LiveData<Int>
    fun countTodayInDelivery(): LiveData<Int>
    fun countThisMonthDelivered(): LiveData<Int>
    fun countInTotalDelivered(): LiveData<Int>

    fun minDelivered(): LiveData<Int>
    fun maxDelivered(): LiveData<Int>
    fun avgDelivered(): LiveData<Int>

    fun getFavouriteLocation(): LiveData<String>

    fun setSmsSent(packageItem: PackageDto)
}
