package com.tiyanak.tant.views.track_an_trace.gps_map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.PersistableBundle
import android.telephony.SmsManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.google.maps.GeoApiContext
import com.google.maps.GeocodingApi
import com.mapbox.android.core.location.LocationEngineProvider
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.api.matching.v5.MapboxMapMatching
import com.mapbox.api.matching.v5.models.MapMatchingResponse
import com.mapbox.geojson.Feature
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute
import com.tiyanak.tant.R
import com.tiyanak.tant.async.AsyncGeocoder
import com.tiyanak.tant.common.constants.DELIVERY
import com.tiyanak.tant.components.adapters.DeliveryAdapter
import com.tiyanak.tant.databinding.ActivityGpsMapBinding
import com.tiyanak.tant.views.track_an_trace.dto.DeliveryDto
import com.tiyanak.tant.views.track_an_trace.gps_navigation.GpsNavigationActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_gps_map.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import kotlin.math.abs

class GpsMapActivity : AppCompatActivity(), OnMapReadyCallback, MapboxMap.OnMapClickListener, PermissionsListener {

    lateinit var viewModel: GpsMapViewModel
    lateinit var deliveryAdapter: DeliveryAdapter

    // variables for adding location layer
    private lateinit var _mapboxMap: MapboxMap
    private lateinit var _mapView: MapView

    // variables for adding location layer
    private lateinit var permissionsManager: PermissionsManager
    private var locationComponent: LocationComponent? = null

    // variables for calculating and drawing a route
    private var currentRoute: MutableLiveData<DirectionsRoute> = MutableLiveData()
    private var navigationMapRoute: NavigationMapRoute? = null

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))

        viewModel = ViewModelProvider(this,
            GpsMapViewModelFactory()
        ).get(GpsMapViewModel::class.java)
        deliveryAdapter = DeliveryAdapter(this)

        val binding: ActivityGpsMapBinding = DataBindingUtil.setContentView(this, R.layout.activity_gps_map)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        _mapView = map_view
        _mapView.onCreate(savedInstanceState)
        _mapView.getMapAsync(this)

        vp_delivery_info.adapter = deliveryAdapter
        vp_delivery_info.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                viewModel.currentPackageIndex.value = position
            }
        })

        viewModel.currentPackageIndex.observe(this, Observer {
            vp_delivery_info.currentItem = it ?: vp_delivery_info.currentItem
        })

        viewModel.deliveriesWithCoords.observe(this, Observer {
            getRoute(it)
        })

        currentRoute.observe(this, Observer {
            drawRoute(it)
        })

        viewModel.deliveriesMediator.observe(this, Observer {  })

        viewModel.navigationEvent.observe(this, Observer {
            val currentPackageIndex = it.getContentIfNotHandledOrReturnNull()
            if (currentPackageIndex == null) {
                startNavigationAll()
            } else {
                startNavigationCurrent(currentPackageIndex)
            }
        })
    }

    private fun drawRoute(route: DirectionsRoute) {
        if (navigationMapRoute != null) {
            navigationMapRoute!!.removeRoute()
        } else {
            navigationMapRoute = NavigationMapRoute(null, _mapView, _mapboxMap, R.style.NavigationMapRoute)
        }
        navigationMapRoute!!.addRoute(route)
    }

    fun getLocation(geocoder: AsyncGeocoder, location: String, callback: AsyncGeocoder.Callback) {
        compositeDisposable.add(geocoder.reverseGeocode(location, callback))
    }

    private fun fetchCoordinates(infos: List<DeliveryDto>) {
        val geocoder = Geocoder(this, Locale.getDefault())
        val geoApiCtx = GeoApiContext.Builder()
            .apiKey(getString(R.string.google_maps_access_token))
            .build()

        GlobalScope.launch(Dispatchers.IO) {
            for (info in infos) {
                try {
                    val addresses = GeocodingApi.geocode(geoApiCtx, info.location).await()
//                    val addresses: List<Address> = geocoder.getFromLocationName(info.location, 1)
                    if (addresses.isNotEmpty()) {
                        val location: com.google.maps.model.LatLng = addresses[0].geometry.location
                        info.longitude = location.lng
                        info.latitude = location.lat

//                        val location: Address = addresses[0]
//                        info.longitude = location.longitude
//                        info.latitude = location.latitude
                    }
                } catch (e: Exception) {
                    Timber.d(e.stackTrace.toString())
                }
            }

            viewModel.deliveriesWithCoords.postValue(sortDeliveryCoords(getMyLocation(), infos.filter { f -> f.longitude != 0.0 }))
        }
    }

    fun getMyLocation(): DeliveryDto {
        val originPoint: Point = Point.fromLngLat(
            locationComponent?.lastKnownLocation?.longitude ?: 0.0,
            locationComponent?.lastKnownLocation?.latitude ?: 0.0
        )
        return DeliveryDto(
            receiver = "",
            location = "",
            phone = "",
            post = 0,
            city = "",
            address = "",
            `package` = "",
            price = 0.0,
            longitude = originPoint.longitude(),
            latitude = originPoint.latitude()
        )
    }

    fun startNavigationAll() {
        val deliveries: Array<DeliveryDto> = viewModel.deliveriesWithCoords.value?.toTypedArray() ?: arrayOf()

        val intent: Intent = Intent(this, GpsNavigationActivity::class.java)
        intent.putExtra(DELIVERY, deliveries)
        startActivity(intent)
    }

    fun startNavigationCurrent(currentDeliveryIndex: Int) {
        val delivery = viewModel.deliveriesWithCoords.value!![currentDeliveryIndex]

        val intent: Intent = Intent(this, GpsNavigationActivity::class.java)
        intent.putExtra(DELIVERY, delivery)
        startActivity(intent)
    }

    private fun sortDeliveryCoords(startingCoord: DeliveryDto, restCoords: List<DeliveryDto>): List<DeliveryDto> {
        var currentCoord = startingCoord

        val sortedCoords: MutableList<DeliveryDto> = mutableListOf()
        val tmpCoords: MutableList<DeliveryDto> = restCoords.toMutableList()

        while (tmpCoords.isNotEmpty()) {
            var minIndex = 0
            var minDistance = 999.0

            for (coordindex in tmpCoords.indices) {
                val thisCoord = tmpCoords[coordindex]
                thisCoord.id = coordindex

                val thisCoordDistance = abs(thisCoord.longitude!! - currentCoord.longitude!!) +
                        abs(thisCoord.latitude!! - currentCoord.latitude!!)

                if (thisCoordDistance < minDistance) {
                    minDistance = thisCoordDistance
                    minIndex = coordindex
                }
            }

            currentCoord = tmpCoords.removeAt(minIndex)
            sortedCoords.add(currentCoord)
        }

        return sortedCoords
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        _mapboxMap = mapboxMap
        _mapboxMap.setStyle(getString(R.string.navigation_guidance_day)) { style ->
            run {
                enableLocationComponent(style)
                addDestinationIconSymbolLayer(style)
                viewModel.deliveries.observe(this, Observer {
                    deliveryAdapter.setItems(it)
                    fetchCoordinates(it)
                })
            }
        }
    }

    private fun addDestinationIconSymbolLayer(loadedMapStyle: Style) {
        loadedMapStyle.addImage(
            "destination-icon-id",
            BitmapFactory.decodeResource(this.resources, R.drawable.mapbox_marker_icon_default)
        )

        val geoJsonSource = GeoJsonSource("destination-source-id")
        loadedMapStyle.addSource(geoJsonSource)

        val destinationSymbolLayer = SymbolLayer("destination-symbol-layer-id", "destination-source-id")
        destinationSymbolLayer.withProperties(
            iconImage("destination-icon-id"),
            iconAllowOverlap(true),
            iconIgnorePlacement(true)
        )

        loadedMapStyle.addLayer(destinationSymbolLayer)
    }

    override fun onMapClick(point: LatLng): Boolean {
        val destinationPoint: Point = Point.fromLngLat(point.longitude, point.latitude)
        val originPoint: Point = Point.fromLngLat(
            locationComponent?.lastKnownLocation?.longitude ?: 0.0,
            locationComponent?.lastKnownLocation?.latitude ?: 0.0
        )

        val source: GeoJsonSource? = _mapboxMap.style?.getSourceAs("destination-source-id")
        source?.setGeoJson(Feature.fromGeometry(destinationPoint))

        return true
    }

    private fun getOptimalRoute(coords: List<DeliveryDto>) {
        if (locationComponent == null) return
        if (!coords.any()) return

        val originPoint: Point = Point.fromLngLat(
            locationComponent?.lastKnownLocation?.longitude ?: 0.0,
            locationComponent?.lastKnownLocation?.latitude ?: 0.0
        )
        val coordinates: List<Point> = arrayListOf(originPoint).plus(coords.map { Point.fromLngLat(it.longitude!!, it.latitude!!) })

        val mapboxMatching = MapboxMapMatching.builder()
            .accessToken(Mapbox.getAccessToken()!!)
            .coordinates(coordinates)
            .steps(true)
            .voiceInstructions(true)
            .bannerInstructions(true)
            .profile(DirectionsCriteria.PROFILE_DRIVING_TRAFFIC)
            .build()
            .enqueueCall(object : Callback<MapMatchingResponse> {
            override fun onResponse(call: Call<MapMatchingResponse>, response: Response<MapMatchingResponse>) {
                if (response.isSuccessful) {
                    currentRoute.postValue(response.body()!!.matchings()!![0].toDirectionRoute())
                }
            }

            override fun onFailure(call: Call<MapMatchingResponse>, throwable: Throwable) {
                Timber.d(throwable)
            }
        })
    }

    private fun getRoute(coords: List<DeliveryDto>) {
        if (locationComponent == null) return
        if (!coords.any()) return

        val originPoint = getMyLocation()

        val sortedCoords = sortDeliveryCoords(originPoint, coords)
        val coordinates: MutableList<Point> = sortedCoords.map { Point.fromLngLat(it.longitude!!, it.latitude!!) }.toMutableList()

        val navRoute = NavigationRoute.builder(this)
            .accessToken(Mapbox.getAccessToken()!!)
            .origin(Point.fromLngLat(originPoint.longitude!!, originPoint.latitude!!))
            .destination(coordinates.removeAt(coordinates.size - 1))
            .profile(DirectionsCriteria.PROFILE_DRIVING)

        coordinates.forEach { navRoute.addWaypoint(it) }

        navRoute.build()
            .getRoute(object: Callback<DirectionsResponse> {
                override fun onResponse(call: Call<DirectionsResponse>, response: Response<DirectionsResponse>) {
                    if (response.body() == null || response.body()!!.routes().size < 1) {
                        return
                    }

                    currentRoute.postValue(response.body()!!.routes()[0])
                }

                override fun onFailure(call: Call<DirectionsResponse>, throwable: Throwable) {
                    Timber.d(throwable)
                }
        })
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            // Activate the MapboxMap LocationComponent to show user location
            // Adding in LocationComponentOptions is also an optional parameter
            locationComponent = _mapboxMap.locationComponent
            val locationOptions = LocationComponentActivationOptions.builder(this, loadedMapStyle)
                .locationEngine(LocationEngineProvider.getBestLocationEngine(this))
                .build()
            locationComponent!!.activateLocationComponent(locationOptions)
            locationComponent!!.isLocationComponentEnabled = true
            // Set the component's camera mode
            locationComponent!!.cameraMode = CameraMode.TRACKING
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocationComponent(_mapboxMap.style!!)
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show()
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        _mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        _mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        _mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        _mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        _mapView.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        _mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        _mapView.onLowMemory()
    }
}