package com.tiyanak.tant.views.track_an_trace.gps_map

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tiyanak.tant.core.usecases.UseCaseFactory
import java.lang.IllegalArgumentException

class GpsMapViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GpsMapViewModel::class.java)) {
            return GpsMapViewModel(
                UseCaseFactory.createPackageUC()
            ) as T
        }
        throw IllegalArgumentException("Unknown View Model");
    }

}