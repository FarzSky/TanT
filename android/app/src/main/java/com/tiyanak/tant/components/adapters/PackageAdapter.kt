package com.tiyanak.tant.components.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.tiyanak.tant.R
import com.tiyanak.tant.common.Event
import com.tiyanak.tant.common.constants.DELIVERED
import com.tiyanak.tant.common.constants.DELIVERY_STATE_MAP
import com.tiyanak.tant.common.constants.IN_DELIVERY
import com.tiyanak.tant.common.constants.IN_STORAGE
import com.tiyanak.tant.components.view_holders.PackageViewHolder
import com.tiyanak.tant.views.packages.package_table.dto.PackageDto
import com.tiyanak.tant.views.packages.package_table.dto.PackageUpdateStateDto

class PackageAdapter(val context: Context) : RecyclerView.Adapter<PackageViewHolder>() {

    private val packages: MutableList<PackageDto> = mutableListOf()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    val packageActionEvent: MutableLiveData<Event<PackageUpdateStateDto>> = MutableLiveData()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PackageViewHolder {
        val itemView = inflater.inflate(R.layout.view_holder_package, parent, false)
        return PackageViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return packages.size
    }

    override fun onBindViewHolder(holder: PackageViewHolder, position: Int) {
        val singlePackage = packages[position]
        holder.bind(singlePackage)
        setActionListener(holder, singlePackage, singlePackage.state)
    }

    fun setPackages(newPackages: List<PackageDto>) {
        packages.clear()
        packages.addAll(newPackages)
        notifyDataSetChanged()
    }

    private fun setActionListener(holder: PackageViewHolder, packageItem: PackageDto, state: String) {
        val btn = holder.itemView.findViewById<Button>(R.id.btn_package_action)
        when (state) {
            DELIVERY_STATE_MAP[IN_STORAGE] -> {
                btn.foreground = context.getDrawable(R.drawable.ic_baseline_add_24)
                btn.setOnClickListener {
                    packageActionEvent.postValue(Event(
                        PackageUpdateStateDto(
                            receiver = packageItem.primatelj,
                            phone = packageItem.telefon,
                            city = packageItem.grad,
                            address = packageItem.adresa,
                            post = packageItem.posta,
                            `package` = packageItem.paket,
                            price = packageItem.cijena.toDouble(),
                            state = IN_DELIVERY
                        )
                    ))
                }
                btn.visibility = VISIBLE
            }
            DELIVERY_STATE_MAP[IN_DELIVERY] -> {
                btn.foreground = context.getDrawable(R.drawable.ic_baseline_remove_24)
                btn.setOnClickListener {
                    packageActionEvent.postValue(Event(
                        PackageUpdateStateDto(
                            receiver = packageItem.primatelj,
                            phone = packageItem.telefon,
                            city = packageItem.grad,
                            address = packageItem.adresa,
                            post = packageItem.posta,
                            `package` = packageItem.paket,
                            price = packageItem.cijena.toDouble(),
                            state = IN_STORAGE
                        )
                    ))
                }
                btn.visibility = VISIBLE
            }
            DELIVERY_STATE_MAP[DELIVERED] -> {
                btn.visibility = GONE
            }
            else -> {
                btn.visibility = GONE
            }
        }
    }
}