package com.tiyanak.tant.views.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.tiyanak.tant.core.usecases.interfaces.IAuthUC
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC

class ProfileViewModel(
    authUC: IAuthUC,
    packageUC: IPackageUC
) : ViewModel() {

    var driver: MutableLiveData<String> = MutableLiveData("")
    var today: MutableLiveData<String> = MutableLiveData("0")
    var thisMonth: MutableLiveData<String> = MutableLiveData("0")
    var inTotal: MutableLiveData<String> = MutableLiveData("0")
    var min: MutableLiveData<String> = MutableLiveData("0")
    var max: MutableLiveData<String> = MutableLiveData("0")
    var avg: MutableLiveData<String> = MutableLiveData("0")
    var favouriteLocation: MutableLiveData<String> = MutableLiveData("-")

    init {
        driver.postValue(authUC.getUserName())

        today = Transformations.map(packageUC.countTodayDelivered()) { count -> count.toString() } as MutableLiveData<String>
        thisMonth = Transformations.map(packageUC.countThisMonthDelivered()) { count -> count.toString() } as MutableLiveData<String>
        inTotal = Transformations.map(packageUC.countInTotalDelivered()) { count -> count.toString() } as MutableLiveData<String>

        min = Transformations.map(packageUC.minDelivered()) { min -> min.toString() } as MutableLiveData<String>
        max = Transformations.map(packageUC.maxDelivered()) { max -> max.toString() } as MutableLiveData<String>
        avg = Transformations.map(packageUC.avgDelivered()) { avg -> avg.toString() } as MutableLiveData<String>

        favouriteLocation = packageUC.getFavouriteLocation() as MutableLiveData<String>
    }

}
