package com.tiyanak.tant.views.packages.create_package

data class CreatePackageDto(
    val primatelj: String,
    val adresa: String,
    val posta: Int,
    val grad: String,
    val telefon: String,
    val paket: String,
    val cijena: String
)