package com.tiyanak.tant.views.signin

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tiyanak.tant.R
import com.tiyanak.tant.core.usecases.interfaces.IAuthUC
import com.tiyanak.tant.data.Result

class SignInViewModel(private val authUC: IAuthUC) : ViewModel() {

    private val _signInDto = MutableLiveData<SignInDto>()
    val signInDto: LiveData<SignInDto> = _signInDto

    private val _signInResult = MutableLiveData<Result<SignedInDto>>()
    val signInResult: LiveData<Result<SignedInDto>> = _signInResult

    fun signIn(username: String, password: String) {
        // can be launched in a separate asynchronous job
        val result = authUC.signIn(username, password)

        try {
            if (result is Result.Success) {
                _signInResult.value =
                    Result.Success(SignedInDto(result.data.username, result.data.email))
            } else {
                _signInResult.value = Result.Error(R.string.signin_failed)
            }
        } catch (e: Exception) {
            println(e.stackTrace)
        }
    }

    fun usernameChanged(username: String) {
        if (!isUserNameValid(username)) {
            _signInDto.value = SignInDto(usernameError = R.string.invalid_username)
        } else {
            _signInDto.value = SignInDto(isDataValid = true)
        }
    }

    fun passwordChanged(password: String) {
        if (!isPasswordValid(password)) {
            _signInDto.value = SignInDto(passwordError = R.string.invalid_password)
        } else {
            _signInDto.value = SignInDto(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(username).matches()
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}
