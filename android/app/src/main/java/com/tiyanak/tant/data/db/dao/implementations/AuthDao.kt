package com.tiyanak.tant.data.db.dao.implementations

import com.tiyanak.tant.R
import com.tiyanak.tant.data.db.entities.SignedInUser
import com.tiyanak.tant.data.Result
import com.tiyanak.tant.data.db.dao.interfaces.IAuthDao

class AuthDao : IAuthDao {
    @Suppress("UNUSED_PARAMETER")
    override fun signIn(username: String, password: String): Result<SignedInUser> {
        return try {
            // handle loggedIn User authentication
            val user = SignedInUser(
                username,
                username
            )
            Result.Success(user)
        } catch (e: Throwable) {
            Result.Error(R.string.signin_failed)
        }
    }

    override fun signout() {
        // revoke authentication
    }
}