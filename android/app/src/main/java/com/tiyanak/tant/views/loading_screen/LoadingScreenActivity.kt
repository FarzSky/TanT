package com.tiyanak.tant.views.loading_screen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.tiyanak.tant.R
import com.tiyanak.tant.core.usecases.UseCaseFactory
import com.tiyanak.tant.core.usecases.interfaces.IAuthUC
import com.tiyanak.tant.views.main.MainActivity
import com.tiyanak.tant.views.signin.SignInActivity
import kotlinx.android.synthetic.main.activity_loading_screen.*

class LoadingScreenActivity : AppCompatActivity() {

    var authUC: IAuthUC = UseCaseFactory.createAuthUC()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading_screen)

        Glide.with(this).load(R.drawable.img_loading_screen).into(img_loading_screen)

        val loadingScreenThread = Thread {
            try {
                Thread.sleep(1000)
            } catch (e: Exception) {
                print(e.message)
            } finally {
                if (!authUC.isSignedIn()) {
                    startActivity(Intent(this, SignInActivity::class.java))
                } else {
                    startActivity(Intent(this, MainActivity::class.java))
                }
                finish()
            }
        }

        loadingScreenThread.start()
    }
}
