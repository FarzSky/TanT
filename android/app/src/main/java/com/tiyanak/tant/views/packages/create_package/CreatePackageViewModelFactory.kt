package com.tiyanak.tant.views.packages.create_package

import androidx.camera.core.UseCase
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tiyanak.tant.App
import com.tiyanak.tant.core.usecases.UseCaseFactory
import com.tiyanak.tant.core.usecases.implementations.PackageUC
import com.tiyanak.tant.data.db.AppDatabase
import java.lang.IllegalArgumentException

class CreatePackageViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreatePackageViewModel::class.java)) {
            return CreatePackageViewModel(UseCaseFactory.createPackageUC()) as T
        }
        throw  IllegalArgumentException("Unknown ViewModel class")
    }
}