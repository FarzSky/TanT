package com.tiyanak.tant.views.track_an_trace.gps_navigation

import androidx.lifecycle.*
import com.tiyanak.tant.common.constants.DELIVERED
import com.tiyanak.tant.common.constants.IN_DELIVERY
import com.tiyanak.tant.common.constants.IN_STORAGE
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC
import com.tiyanak.tant.views.packages.package_table.dto.PackageDto
import com.tiyanak.tant.views.packages.package_table.dto.PackageUpdateStateDto
import com.tiyanak.tant.views.track_an_trace.dto.DeliveryDto
import java.util.*

class GpsNavigationViewModel(val packageUC: IPackageUC) : ViewModel() {

    private val _delivery: MutableLiveData<List<DeliveryDto>> = MutableLiveData()
    val delivery: LiveData<List<DeliveryDto>> = _delivery
    val deliveryInfosMediator = MediatorLiveData<Unit>()

    var currentPackageIndex: MutableLiveData<Int> = MutableLiveData(0)

    init { }

    fun setDeliveries(deliveries: List<DeliveryDto>) {
        _delivery.postValue(deliveries)
    }

    fun previousPackage() {
        val currentIndex = currentPackageIndex.value ?: 0
        if (currentIndex <= 0) return

        currentPackageIndex.postValue(currentPackageIndex.value?.dec())
    }

    fun nextPackage() {
        val currentIndex = currentPackageIndex.value ?: 0
        if (currentIndex + 1 >= (_delivery.value?.size ?: 0)) return

        currentPackageIndex.postValue(currentPackageIndex.value?.inc())
    }

    fun onDeliveryConfirmed() {
        val confirmedPackage = currentPackageIndex.value?.let { delivery.value?.get(it) }
        confirmedPackage?.let {
            packageUC.updatePackageState(
                PackageUpdateStateDto(
                    receiver = it.receiver,
                    phone = it.phone,
                    city = it.city,
                    address = it.address,
                    post = it.post,
                    `package` = it.`package`,
                    price = it.price,
                    state = DELIVERED,
                    deliveredAt = Date()
                )
            )
        }
    }

    fun onCancelDelivery() {
        val canceledPackage = currentPackageIndex.value?.let { delivery.value?.get(it) }
        canceledPackage?.let {
            packageUC.updatePackageState(
                PackageUpdateStateDto(
                    receiver = it.receiver,
                    phone = it.phone,
                    city = it.city,
                    address = it.address,
                    post = it.post,
                    `package` = it.`package`,
                    price = it.price,
                    state = IN_STORAGE,
                    deliveredAt = Date()
                )
            )
        }
    }

    fun setSentSms(deliveryDto: DeliveryDto) {
        packageUC.setSmsSent(
            PackageDto(
                primatelj = deliveryDto.receiver,
                telefon = deliveryDto.phone,
                lokacija = deliveryDto.location,
                paket = deliveryDto.`package`,
                cijena = deliveryDto.price.toString(),
                state = IN_DELIVERY,
                posta = deliveryDto.post,
                adresa = deliveryDto.address,
                grad = deliveryDto.city,
                smsSent = deliveryDto.smsSent
            )
        )
    }
}