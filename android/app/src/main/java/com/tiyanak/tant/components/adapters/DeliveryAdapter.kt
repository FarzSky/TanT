package com.tiyanak.tant.components.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tiyanak.tant.R
import com.tiyanak.tant.components.view_holders.DeliveryInfoViewHolder
import com.tiyanak.tant.views.track_an_trace.dto.DeliveryDto

class DeliveryAdapter(val context: Context) : RecyclerView.Adapter<DeliveryInfoViewHolder>() {

    private val deliveries: MutableList<DeliveryDto> = mutableListOf()

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeliveryInfoViewHolder {
        val itemView = inflater.inflate(R.layout.view_holder_delivery_info, parent, false)
        return DeliveryInfoViewHolder(itemView)
    }

    override fun onBindViewHolder(viewHolder: DeliveryInfoViewHolder, position: Int) {
        val deliveryInfo = deliveries[position]
        viewHolder.bind(deliveryInfo)
    }

    override fun getItemViewType(position: Int): Int {
        return 1
    }

    override fun getItemCount(): Int {
        return deliveries.size
    }

    fun setItems(newItems: List<DeliveryDto>) {
        deliveries.clear()
        deliveries.addAll(newItems)
        notifyDataSetChanged()
    }
}