package com.tiyanak.tant.views.packages.package_table.packages

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tiyanak.tant.App
import com.tiyanak.tant.core.usecases.UseCaseFactory
import com.tiyanak.tant.data.db.AppDatabase

class PackagesViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PackagesViewModel::class.java)) {
            return PackagesViewModel(UseCaseFactory.createPackageUC()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}