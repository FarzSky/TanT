package com.tiyanak.tant.views.track_an_trace.gps_navigation

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.PersistableBundle
import android.telephony.SmsManager
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.snackbar.Snackbar
import com.mapbox.android.core.location.LocationEngineCallback
import com.mapbox.android.core.location.LocationEngineProvider
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.core.constants.Constants
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.annotations.Polyline
import com.mapbox.mapboxsdk.annotations.PolylineOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.services.android.navigation.v5.location.replay.ReplayRouteLocationEngine
import com.mapbox.services.android.navigation.v5.milestone.Milestone
import com.mapbox.services.android.navigation.v5.milestone.MilestoneEventListener
import com.mapbox.services.android.navigation.v5.milestone.VoiceInstructionMilestone
import com.mapbox.services.android.navigation.v5.navigation.MapboxNavigation
import com.mapbox.services.android.navigation.v5.navigation.MapboxNavigationOptions
import com.mapbox.services.android.navigation.v5.navigation.NavigationEventListener
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute
import com.mapbox.services.android.navigation.v5.offroute.OffRouteListener
import com.mapbox.services.android.navigation.v5.routeprogress.ProgressChangeListener
import com.mapbox.services.android.navigation.v5.routeprogress.RouteProgress
import com.tiyanak.tant.R
import com.tiyanak.tant.common.constants.DELIVERY
import com.tiyanak.tant.common.convertSecondsToTime
import com.tiyanak.tant.common.round
import com.tiyanak.tant.components.adapters.DeliveryAdapter
import com.tiyanak.tant.databinding.ActivityGpsNavigationBinding
import com.tiyanak.tant.views.track_an_trace.dto.DeliveryDto
import kotlinx.android.synthetic.main.activity_gps_map.map_view
import kotlinx.android.synthetic.main.activity_gps_map.vp_delivery_info
import kotlinx.android.synthetic.main.activity_gps_navigation.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class GpsNavigationActivity : AppCompatActivity(), OnMapReadyCallback,
    Callback<DirectionsResponse>, MapboxMap.OnMapClickListener, NavigationEventListener,
    OffRouteListener, ProgressChangeListener, MilestoneEventListener {

    lateinit var viewModel: GpsNavigationViewModel
    lateinit var deliveryAdapter: DeliveryAdapter

    // variables for adding location layer
    private lateinit var _mapboxMap: MapboxMap
    private lateinit var _mapView: MapView
    private lateinit var contentLayout: View

    private lateinit var navigation: MapboxNavigation
    private var mockLocationEngine: ReplayRouteLocationEngine? = null

    private val callback: RerouteActivityLocationCallback = RerouteActivityLocationCallback(this)

    // variables for adding location layer
    private lateinit var  locationComponent: LocationComponent

    private var origin: Point = Point.fromLngLat(-0.358764, 39.494876)
    private var tracking = false
    private var running = false
    private var lastLocation: Location? = null
    private var polyline: Polyline? = null
    private var wasInTunnel = false

    private val MY_PERMISSIONS_REQUEST_SEND_SMS: Int = 505

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))

        viewModel = ViewModelProvider(this, GpsNavigationViewModelFactory()).get(GpsNavigationViewModel::class.java)
        deliveryAdapter = DeliveryAdapter(this)

        val binding: ActivityGpsNavigationBinding = DataBindingUtil.setContentView(this, R.layout.activity_gps_navigation)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        contentLayout = findViewById(android.R.id.content)

        val options = MapboxNavigationOptions.builder().build()
        navigation = MapboxNavigation(applicationContext, Mapbox.getAccessToken()!!, options)
        navigation.addNavigationEventListener(this)
        navigation.addMilestoneEventListener(this)

        vp_delivery_info.adapter = deliveryAdapter
        vp_delivery_info.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                viewModel.currentPackageIndex.value = position
            }
        })

        viewModel.currentPackageIndex.observe(this, Observer {
            vp_delivery_info.currentItem = it ?: vp_delivery_info.currentItem
        })

        intent.extras?.getParcelableArray(DELIVERY)?.let {
            viewModel.setDeliveries(it.map { parc -> parc as DeliveryDto })
        }

        viewModel.deliveryInfosMediator.observe(this, Observer {  })

        _mapView = map_view
        _mapView.onCreate(savedInstanceState)
        _mapView.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(mapboxMap: MapboxMap) {
        _mapboxMap = mapboxMap
        mapboxMap.addOnMapClickListener(this)
        mapboxMap.setStyle(Style.DARK) { style ->
            buildLocationComponent(style)
            viewModel.delivery.observe(this, Observer { it ->
                deliveryAdapter.setItems(it)
                getRoute(it)
            })
        }
    }

    @SuppressLint("MissingPermission")
    fun buildLocationComponent(style: Style) {
        locationComponent = _mapboxMap.locationComponent
        val locationOptions = LocationComponentActivationOptions.builder(this, style)
            .locationEngine(LocationEngineProvider.getBestLocationEngine(this))
            .build()
        locationComponent.activateLocationComponent(locationOptions)
        locationComponent.isLocationComponentEnabled = true
        locationComponent.renderMode = RenderMode.GPS
        mockLocationEngine = ReplayRouteLocationEngine()
    }

    override fun onMapClick(point: LatLng): Boolean {
        if (!running || lastLocation == null) {
            return false
        }

        _mapboxMap.addMarker(MarkerOptions().position(point))
        _mapboxMap.removeOnMapClickListener(this)
        val destination = Point.fromLngLat(point.getLongitude(), point.getLatitude())
        resetLocationEngine(destination)
        tracking = false

        return false
    }

    private fun getRoute(coords: List<DeliveryDto>) {
        if (!coords.any()) return

        origin = Point.fromLngLat(
            locationComponent.lastKnownLocation?.longitude ?: 0.0,
            locationComponent.lastKnownLocation?.latitude ?: 0.0
        )

        val coordinates: MutableList<Point> = coords.map { Point.fromLngLat(it.longitude!!, it.latitude!!) }.toMutableList()

        val navRoute = NavigationRoute.builder(this)
            .accessToken(Mapbox.getAccessToken()!!)
            .origin(origin)
            .destination(coordinates.removeAt(coordinates.size - 1))
            .profile(DirectionsCriteria.PROFILE_DRIVING)

        coordinates.forEach { navRoute.addWaypoint(it) }

        navRoute.build().getRoute(this)
    }

    private fun drawRoute(route: DirectionsRoute) {
        val points: MutableList<LatLng> = ArrayList()
        val coords: MutableList<Point> = LineString.fromPolyline(route.geometry().toString(), Constants.PRECISION_6).coordinates()
        for (point in coords) {
            points.add(LatLng(point.latitude(), point.longitude()))
        }

        if (points.isNotEmpty()) {
            if (polyline != null) {
                _mapboxMap.removePolyline(polyline!!)
            }
            polyline = _mapboxMap.addPolyline(
                PolylineOptions()
                    .addAll(points)
                    .color(Color.parseColor(getString(R.string.mapbox_blue)))
                    .width(5F)
            )
        }
    }

    override fun onProgressChange(location: Location,  routeProgress: RouteProgress) {
        val isInTunnel: Boolean = routeProgress.inTunnel()
        lastLocation = location

        if (!wasInTunnel && isInTunnel) {
            wasInTunnel = true
            Snackbar.make(contentLayout, "Enter tunnel!", Snackbar.LENGTH_SHORT).show()
        }

        if (wasInTunnel && !isInTunnel) {
            wasInTunnel = false
            Snackbar.make(contentLayout, "Exit tunnel!", Snackbar.LENGTH_SHORT).show()
        }

        if (tracking) {
            _mapboxMap.locationComponent.forceLocationUpdate(location)
            val cameraPosition: CameraPosition = CameraPosition.Builder()
                .zoom(15.0)
                .target(
                    LatLng(
                        location.latitude,
                        location.longitude
                    )
                )
                .bearing(location.bearing.toDouble())
                .build()
            _mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000)
        }

        tv_distance_left.text = routeProgress.currentLegProgress().distanceRemaining().div(1000.0).round(2).toString()
        tv_duration_left.text = routeProgress.currentLegProgress().durationRemaining().round(0).toLong().convertSecondsToTime()
//        routeProgress.bannerInstruction() ?.primary?.let {
//            tv_instruction.text = "${it.type} ${it.modifier} to ${it.text}"
//        }
    }

    fun updateLocation(location: Location?) {
        if (!tracking) {
            _mapboxMap.locationComponent.forceLocationUpdate(location)
        }
    }

    override fun onResponse(call: Call<DirectionsResponse?>, response: Response<DirectionsResponse?>) {
        Timber.d(call.request().url().toString())
        if (response.body() != null) {
            if (response.body()!!.routes().isNotEmpty()) {
                val route: DirectionsRoute = response.body()!!.routes()[0]
                drawRoute(route)
                resetLocationEngine(route)
                navigation.startNavigation(route)
                _mapboxMap.addOnMapClickListener(this)
                tracking = true

                route.legs()?.let {
                    var duration = 0.0
                    it.indices.forEach { index ->
                        duration += it[index].duration() ?: 0.0
                        viewModel.delivery.value?.let { deliveryItems ->
                            deliveryItems[index].duration = duration
                        }
                    }
                }

                requestToSendSMS()
            }
        }
    }

    override fun onFailure(call: Call<DirectionsResponse?>, throwable: Throwable) {
        Timber.d(throwable)
    }

    override fun onMilestoneEvent(routeProgress: RouteProgress?, instruction: String?, milestone: Milestone?) {
        if (milestone is VoiceInstructionMilestone) {
//            Snackbar.make(contentLayout, instruction.toString(), Snackbar.LENGTH_SHORT).show()
            tv_instruction.text = instruction
        }
    }

    override fun onRunning(isRunning: Boolean) {
        running = isRunning
        if (running) {
            navigation.addOffRouteListener(this)
            navigation.addProgressChangeListener(this)
        }
    }

    private fun shutdownLocationEngine() {
        mockLocationEngine?.removeLocationUpdates(callback)
    }

    private fun resetLocationEngine(point: Point) {
        mockLocationEngine!!.moveTo(point)
        navigation.locationEngine = mockLocationEngine!!
    }

    private fun resetLocationEngine(directionsRoute: DirectionsRoute) {
        mockLocationEngine!!.assign(directionsRoute)
        navigation.locationEngine = mockLocationEngine!!
    }

    private fun shutdownNavigation() {
        navigation.removeNavigationEventListener(this)
        navigation.removeProgressChangeListener(this)
        navigation.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        _mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        _mapView.onStart()
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        _mapView.onSaveInstanceState(outState)
    }

    override fun onStop() {
        super.onStop()
        _mapView.onStop()
    }

    override fun onPause() {
        super.onPause()
        _mapView.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        _mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        _mapView.onDestroy()
        shutdownLocationEngine()
        shutdownNavigation()
    }

    private class RerouteActivityLocationCallback internal constructor(activity: GpsNavigationActivity?) :
        LocationEngineCallback<LocationEngineResult?> {

        private val activityWeakReference: WeakReference<GpsNavigationActivity?>?

        override fun onSuccess(result: LocationEngineResult?) {
            val activity: GpsNavigationActivity? = activityWeakReference?.get()
            if (activity != null) {
                val location: Location = result?.lastLocation ?: return
                activity.updateLocation(location)
            }
        }

        override fun onFailure(exception: Exception) {
            Timber.e(exception)
        }

        init {
            activityWeakReference = WeakReference(activity)
        }
    }

    override fun userOffRoute(location: Location) {
        getRoute(viewModel.delivery.value!!)
        Snackbar.make(contentLayout, "User Off Route", Snackbar.LENGTH_SHORT).show()
        _mapboxMap.addMarker(
            MarkerOptions().position(
                LatLng(
                    location.latitude,
                    location.longitude
                )
            )
        )
    }

    protected fun requestToSendSMS() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            // You can use the API that requires the permission.
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
            } else {
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.SEND_SMS),
                    MY_PERMISSIONS_REQUEST_SEND_SMS
                )
            }
        } else {
            sendSMS()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            MY_PERMISSIONS_REQUEST_SEND_SMS -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSMS()
                } else {
                    Toast.makeText(applicationContext,"SMS failed, please try again.", Toast.LENGTH_LONG).show()
                    return
                }
            }
        }
    }

    private fun sendSMS() {
        viewModel.delivery.value?.forEach {
            if (it.smsSent == 0){
                val duration = convertToTime(it.duration!!)
                val smsMessage = "Poštovani ${it.receiver}, vaš paket (${it.`package`}) će biti dostavljen danas na adresu ${it.address}, ${it.city} između ${duration} sati."
                val phoneNo = it.phone
                val smsManager: SmsManager = SmsManager.getDefault()
                smsManager.sendTextMessage(phoneNo, null, smsMessage, null, null)
                it.smsSent = 1
                viewModel.setSentSms(it)
            }
        }
        Toast.makeText(applicationContext, "SMS sent.", Toast.LENGTH_LONG).show()
    }

    private fun convertToTime(duration: Double): String {
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.add(Calendar.SECOND, duration.toInt())

        var result: String = ""

        val pattern = "HH:mm"
        val simpleDateFormat = SimpleDateFormat(pattern)
        result += simpleDateFormat.format(calendar.time)

        result += " i "

        calendar.add(Calendar.HOUR, 2)
        result += simpleDateFormat.format(calendar.time)

        return result
    }


}