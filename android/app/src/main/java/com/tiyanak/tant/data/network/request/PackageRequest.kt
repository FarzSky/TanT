package com.tiyanak.tant.data.network.request

data class PackageRequest(
    val primatelj: String,
    val adresa: String,
    val posta: Int,
    val grad: String,
    val telefon: String,
    val paket: String,
    val cijena: Double
)