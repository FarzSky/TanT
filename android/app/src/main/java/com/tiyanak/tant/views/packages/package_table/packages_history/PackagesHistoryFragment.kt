package com.tiyanak.tant.views.packages.package_table.packages_history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tiyanak.tant.R
import com.tiyanak.tant.components.adapters.PackageAdapter

class PackagesHistoryFragment : Fragment() {

    private lateinit var viewModel: PackagesHistoryViewModel

    private lateinit var packagesHistoryAdapter: PackageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, PackagesHistoryViewModelFactory()).get(PackagesHistoryViewModel::class.java)
        packagesHistoryAdapter = PackageAdapter(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_packages_history, container, false)

        val rv = root.findViewById<RecyclerView>(R.id.rv_packages_history)
        rv.setHasFixedSize(true)
        rv.setItemViewCacheSize(20)
        rv.itemAnimator = DefaultItemAnimator()
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = packagesHistoryAdapter

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.mediator.observe(viewLifecycleOwner, Observer {  })
        viewModel.packages.observe(viewLifecycleOwner, Observer {
            packagesHistoryAdapter.setPackages(it)
        })
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): PackagesHistoryFragment {
            return PackagesHistoryFragment()
                .apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}