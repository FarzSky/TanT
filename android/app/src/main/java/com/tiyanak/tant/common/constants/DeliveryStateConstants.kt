package com.tiyanak.tant.common.constants

const val IN_STORAGE = "in_storage"
const val DELIVERED = "delivered"
const val IN_DELIVERY = "in_delivery"
const val UNKNOWN = "Unknown"

val DELIVERY_STATE_MAP: Map<String, String> = mapOf(IN_STORAGE to "In storage", DELIVERED to "Delivered", IN_DELIVERY to "In delivery")