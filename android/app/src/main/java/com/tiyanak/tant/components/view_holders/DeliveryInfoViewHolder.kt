package com.tiyanak.tant.components.view_holders

import android.view.View
import android.widget.TextView
import com.tiyanak.tant.R
import com.tiyanak.tant.views.track_an_trace.dto.DeliveryDto

class DeliveryInfoViewHolder(itemView: View) : BaseViewHolder<DeliveryDto>(itemView) {
    override fun bind(item: DeliveryDto) {
        itemView.findViewById<TextView>(R.id.tv_receiver).text = item.receiver
        itemView.findViewById<TextView>(R.id.tv_location).text = item.location
        itemView.findViewById<TextView>(R.id.tv_phone).text = item.phone
        itemView.findViewById<TextView>(R.id.tv_package).text = item.`package`
    }
}