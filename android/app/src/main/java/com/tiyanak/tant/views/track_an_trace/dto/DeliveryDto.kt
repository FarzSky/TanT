package com.tiyanak.tant.views.track_an_trace.dto

import android.os.Parcel
import android.os.Parcelable
import com.mapbox.geojson.Point

data class DeliveryDto(
    var id: Int? = -1,
    val receiver: String,
    val location: String,
    val phone: String,
    val post: Int,
    val city: String,
    val address: String,
    val `package`: String,
    val price: Double,
    var longitude: Double? = 0.0,
    var latitude: Double? = 0.0,
    var smsSent: Int = 0,
    var duration: Double? = 0.0
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readDouble()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id!!)
        parcel.writeString(receiver)
        parcel.writeString(location)
        parcel.writeString(phone)
        parcel.writeInt(post)
        parcel.writeString(city)
        parcel.writeString(address)
        parcel.writeString(`package`)
        parcel.writeDouble(price)
        parcel.writeDouble(longitude!!)
        parcel.writeDouble(latitude!!)
        parcel.writeInt(smsSent)
        parcel.writeDouble(duration!!)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DeliveryDto> {
        override fun createFromParcel(parcel: Parcel): DeliveryDto {
            return DeliveryDto(parcel)
        }

        override fun newArray(size: Int): Array<DeliveryDto?> {
            return arrayOfNulls(size)
        }
    }
}