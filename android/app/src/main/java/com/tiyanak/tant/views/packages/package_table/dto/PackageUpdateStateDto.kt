package com.tiyanak.tant.views.packages.package_table.dto

import java.util.*

data class PackageUpdateStateDto(
    val receiver: String,
    val phone: String,
    val city: String,
    val address: String,
    val post: Int,
    val `package`: String,
    val price: Double,
    val state: String,
    val deliveredAt: Date? = null
)