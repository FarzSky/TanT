package com.tiyanak.tant.views.track_an_trace.trace_packages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tiyanak.tant.common.Event
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC
import com.tiyanak.tant.views.packages.package_table.dto.PackageDto
import com.tiyanak.tant.views.packages.package_table.dto.PackageUpdateStateDto

class TracePackagesViewModel(val packageUC: IPackageUC) : ViewModel() {
    private val _packages: MutableLiveData<MutableList<PackageDto>> = MutableLiveData(mutableListOf())
    val packages: LiveData<MutableList<PackageDto>> = _packages

    val mediator = MediatorLiveData<Unit>()

    val startNavigationEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()

    init {
        mediator.addSource(packageUC.getInDeliveryPackages()) {
            _packages.value = it.toMutableList()
        }
    }

    fun startNavigation() {
        startNavigationEvent.postValue(Event(true))
    }

    fun updatePackageState(newPackageState: PackageUpdateStateDto) {
        packageUC.updatePackageState(newPackageState)
    }
}