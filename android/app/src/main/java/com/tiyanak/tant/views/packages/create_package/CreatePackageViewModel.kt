package com.tiyanak.tant.views.packages.create_package

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tiyanak.tant.common.Event
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC

class CreatePackageViewModel(val packagesUC: IPackageUC) : ViewModel() {

    val primatelj: MutableLiveData<String> = MutableLiveData("")
    val telefon: MutableLiveData<String> = MutableLiveData("")
    val grad: MutableLiveData<String> = MutableLiveData("")
    val posta: MutableLiveData<String> = MutableLiveData("")
    val adresa: MutableLiveData<String> = MutableLiveData("")
    val paket: MutableLiveData<String> = MutableLiveData("")
    val cijena: MutableLiveData<String> = MutableLiveData("")

    val packageCreationEvent: MutableLiveData<Event<Boolean>> = MutableLiveData()

    fun onCreateClicked() {
        packagesUC.createPackage(CreatePackageDto(
            primatelj = primatelj.value!!,
            adresa = adresa.value!!,
            posta = posta.value!!.toInt(),
            grad = grad.value!!,
            telefon = telefon.value!!,
            paket = paket.value!!,
            cijena = cijena.value!!
        ))
        packageCreationEvent.postValue(Event(true))
    }

    fun onCancelClicked() {
        packageCreationEvent.postValue(Event(false))
    }
}