package com.tiyanak.tant.data.network.api

import com.tiyanak.tant.data.network.request.PackageRequest
import com.tiyanak.tant.data.network.request.UpdatePackageRequest
import com.tiyanak.tant.data.network.response.PackageResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import timber.log.Timber

interface PackageApi {

    @GET("packages/origin")
    fun getOriginPackages(): Call<List<PackageResponse>>

    @GET("packages/copy")
    fun getCopyPackages(): Call<List<PackageResponse>>

    @POST("packages/copy")
    fun createCopyPackage(@Body packageRequest: PackageRequest): Call<Void>

    @PUT("packages/copy")
    fun updateCopyPackage(@Body packageRequest: UpdatePackageRequest): Call<Void>

    @POST("packages/copy/delete")
    fun deletePackage(@Body packageRequest: UpdatePackageRequest): Call<Void>

    companion object Factory {
        fun create(): PackageApi {
            val logger = HttpLoggingInterceptor(object: HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Timber.tag("Package Api call: ").d(message)
                }
            })

            logger.setLevel(HttpLoggingInterceptor.Level.BODY)

            val okHttpClientBuilder = OkHttpClient.Builder()
            okHttpClientBuilder.addInterceptor(logger)

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://192.168.2.100:5000")
                .client(okHttpClientBuilder.build())
                .build()

            return retrofit.create(PackageApi::class.java)
        }
    }

}