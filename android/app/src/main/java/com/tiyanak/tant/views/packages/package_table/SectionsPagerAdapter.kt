package com.tiyanak.tant.views.packages.package_table

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.tiyanak.tant.R
import com.tiyanak.tant.views.packages.package_table.packages.PackagesFragment
import com.tiyanak.tant.views.packages.package_table.packages_history.PackagesHistoryFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_packages,
    R.string.tab_packages_history
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        when (position) {
            0 -> return PackagesFragment.newInstance(position)
            1 -> return PackagesHistoryFragment.newInstance(position)
            else -> return PackagesFragment.newInstance(position)
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return TAB_TITLES.size
    }
}