package com.tiyanak.tant.views.packages.create_package

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.datepicker.MaterialDatePicker.Builder.datePicker
import com.tiyanak.tant.R
import com.tiyanak.tant.databinding.ActivityCreatePackageBinding
import kotlinx.android.synthetic.main.activity_create_package.*
import java.util.*


class CreatePackageActivity : AppCompatActivity() {

    private lateinit var viewModel: CreatePackageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, CreatePackageViewModelFactory()).get(CreatePackageViewModel::class.java)

        val binding: ActivityCreatePackageBinding =  DataBindingUtil.setContentView(this, R.layout.activity_create_package)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        btn_create.setOnClickListener {
            onCreateClicked()
        }

        viewModel.packageCreationEvent.observe(this, Observer {
            event -> event.getContentIfNotHandledOrReturnNull()?.let {
                finish()
            }
        })
    }

    // because of datepicker, this function is not possible to have it in viewmodel
    private fun onCreateClicked() {
        viewModel.onCreateClicked()
    }
}
