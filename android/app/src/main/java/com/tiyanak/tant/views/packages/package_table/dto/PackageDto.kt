package com.tiyanak.tant.views.packages.package_table.dto

data class PackageDto(
    val primatelj: String,
    val telefon: String,
    val lokacija: String,
    val paket: String,
    val cijena: String,
    val state: String,
    val posta: Int,
    val adresa: String,
    val grad: String,
    val smsSent: Int = 0
)