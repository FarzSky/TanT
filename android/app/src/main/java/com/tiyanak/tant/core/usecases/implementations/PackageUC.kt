package com.tiyanak.tant.core.usecases.implementations

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.tiyanak.tant.common.constants.*
import com.tiyanak.tant.common.converters.*
import com.tiyanak.tant.data.db.entities.Package
import com.tiyanak.tant.core.usecases.interfaces.IAuthUC
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC
import com.tiyanak.tant.data.db.AppDatabase
import com.tiyanak.tant.data.db.dao.interfaces.IPackageDao
import com.tiyanak.tant.data.network.api.PackageApi
import com.tiyanak.tant.data.network.response.PackageResponse
import com.tiyanak.tant.views.packages.create_package.CreatePackageDto
import com.tiyanak.tant.views.packages.package_table.dto.PackageDto
import com.tiyanak.tant.views.packages.package_table.dto.PackageUpdateStateDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class PackageUC(
    val packageDao: IPackageDao,
    val authUC: IAuthUC
) : IPackageUC {

    val packageApi: PackageApi = PackageApi.create()

    override fun createPackage(createPackageDto: CreatePackageDto) {
        val packageE = Package(
            id = null,
            primatelj = createPackageDto.primatelj,
            adresa = createPackageDto.adresa,
            posta = createPackageDto.posta,
            grad = createPackageDto.grad,
            telefon = createPackageDto.telefon,
            paket = createPackageDto.paket,
            cijena = createPackageDto.cijena.toDouble(),
            state = IN_DELIVERY
        )

        AppDatabase.databaseWriteExecutor.execute{
            packageDao.createPackage(packageE)
        }
    }

    override fun getInStoragePackages(): MutableLiveData<List<PackageDto>> {
        val packagesResponse: MutableLiveData<List<PackageDto>> = MutableLiveData()

        packageApi.getCopyPackages().enqueue(object : Callback<List<PackageResponse>> {
            override fun onResponse(call: Call<List<PackageResponse>>?, response: Response<List<PackageResponse>>?) {
                if (response != null && response.isSuccessful) {
                    val responseData: List<PackageResponse>? = response.body()
                    responseData?.let {
                        GlobalScope.launch(Dispatchers.IO) {
                            val localPackages = packageDao.getAll()
                            packagesResponse.postValue(mapPackageResponseToDto(it.filter {
                                    f -> !localPackages.any { a -> a.posta == f.posta && a.primatelj == f.primatelj && a.adresa == f.adresa && a.paket == f.paket }
                            }).toMutableList())
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<PackageResponse>>?, t: Throwable?) { }
        })

        return packagesResponse
    }

    override fun getDeliveredPackages(): LiveData<List<PackageDto>> {
        return Transformations.switchMap(packageDao.getPackagesByStatus(DELIVERED), this::mapPackageEntityToDto)
    }

    override fun getInDeliveryPackages(): LiveData<List<PackageDto>> {
        return Transformations.switchMap(packageDao.getPackagesByStatus(IN_DELIVERY), this::mapPackageEntityToDto)
    }

    override fun updatePackageState(newPackageState: PackageUpdateStateDto) {
        GlobalScope.launch(Dispatchers.IO) {
            if (newPackageState.state == IN_STORAGE) {
                packageDao.deletePackage(newPackageState.receiver, newPackageState.post, newPackageState.address, newPackageState.`package`)
            } else {
                if (packageDao.exists(newPackageState.receiver, newPackageState.post, newPackageState.address, newPackageState.`package`)) {
                    packageDao.updatePackageState(newPackageState.receiver, newPackageState.post, newPackageState.address, newPackageState.`package`, newPackageState.state)
                } else {
                    packageDao.createPackage(Package(
                        primatelj = newPackageState.receiver,
                        adresa = newPackageState.address,
                        posta = newPackageState.post,
                        grad = newPackageState.city,
                        telefon = newPackageState.phone,
                        paket = newPackageState.`package`,
                        cijena = newPackageState.price,
                        state = newPackageState.state,
                        deliveredAt = newPackageState.deliveredAt
                    ))
                }
            }
        }
    }

    override fun countTodayDelivered(): LiveData<Int> {
        val todayDelivered = packageDao.getByDateDeliveredAndStatus(Date(), Date(), DELIVERED)
        return Transformations.switchMap(todayDelivered) { packages -> MutableLiveData(packages.size) }
    }

    override fun countThisMonthDelivered(): LiveData<Int> {
        val thisMonth = packageDao.getByDateDeliveredAndStatus(toStartOfMonth(Date()), toEndOfMonth(Date()), DELIVERED)
        return Transformations.switchMap(thisMonth) { packages -> MutableLiveData(packages.size) }
    }

    override fun countInTotalDelivered(): LiveData<Int> {
        val inTotal = packageDao.getByDateDeliveredAndStatus(toStartOfYear(Date()), toEndOfYear(Date()), DELIVERED)
        return Transformations.switchMap(inTotal) { packages -> MutableLiveData(packages.size) }
    }

    override fun countTodayInDelivery(): LiveData<Int> {
        val todayDelivered = packageDao.getPackagesByStatus(IN_DELIVERY)
        return Transformations.switchMap(todayDelivered) { packages -> MutableLiveData(packages.size) }
    }

    override fun minDelivered(): LiveData<Int> {
        return Transformations.switchMap(packageDao.countPerDay()) { counts -> MutableLiveData(counts.min() ?: 0) }
    }

    override fun maxDelivered(): LiveData<Int> {
        return Transformations.switchMap(packageDao.countPerDay()) { counts -> MutableLiveData(counts.max() ?: 0) }
    }

    override fun avgDelivered(): LiveData<Int> {
        return Transformations.switchMap(packageDao.countPerDay()) { counts -> MutableLiveData(counts.average().toInt()) }
    }

    override fun setSmsSent(packageItem: PackageDto) {
        GlobalScope.launch(Dispatchers.IO) {
            packageDao.setSmsSent(packageItem.primatelj, packageItem.posta, packageItem.adresa, packageItem.paket)
        }
    }

    override fun getFavouriteLocation(): LiveData<String> {
        return Transformations.switchMap(packageDao.getFavouriteLocation()) {
            locations -> MutableLiveData(
            locations.maxBy { location -> location.city }?.city ?: "-")
            }
    }

    private fun mapPackageEntityToDto(packages: List<Package>): LiveData<List<PackageDto>> {
        val mappedPackages = packages.map {
            PackageDto(
                primatelj = it.primatelj,
                telefon = it.telefon ?: "",
                lokacija = it.grad + " " + it.posta + ", " + it.adresa,
                paket = it.paket ?: "",
                cijena = it.cijena?.toString() ?: "0",
                state = DELIVERY_STATE_MAP.getOrElse(it.state) { UNKNOWN },
                posta = it.posta,
                adresa = it.adresa,
                grad = it.grad,
                smsSent = it.smsSent
            )
        }

        return  MutableLiveData(mappedPackages)
    }

    private fun mapPackageResponseToDto(packages: List<PackageResponse>): List<PackageDto> {
        return packages.map {
            PackageDto(
                primatelj = it.primatelj,
                telefon = it.telefon,
                lokacija = it.grad + " " + it.posta + ", " + it.adresa,
                paket = it.paket,
                cijena = it.cijena.toString(),
                state = DELIVERY_STATE_MAP.getOrElse(IN_STORAGE) { IN_STORAGE },
                posta = it.posta,
                adresa = it.adresa,
                grad = it.grad
            )
        }
    }
}