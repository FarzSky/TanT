package com.tiyanak.tant.views.track_an_trace.trace_packages

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tiyanak.tant.R
import com.tiyanak.tant.common.constants.INTENT_ACTIVITY_KEY
import com.tiyanak.tant.common.constants.TRACE_PACKAGES_FRAGMENT
import com.tiyanak.tant.components.adapters.PackageAdapter
import com.tiyanak.tant.databinding.FragmentTracePackagesBinding
import com.tiyanak.tant.views.track_an_trace.gps_map.GpsMapActivity

class TracePackagesFragment : Fragment() {

    companion object {
        fun newInstance() = TracePackagesFragment()
    }

    private lateinit var viewModel: TracePackagesViewModel
    private lateinit var packagesAdapter: PackageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, TracePackagesViewModelFactory()).get(TracePackagesViewModel::class.java)
        packagesAdapter = PackageAdapter(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentTracePackagesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trace_packages, container, false)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel

        val rv = binding.root.findViewById<RecyclerView>(R.id.rv_trace_packages)
        rv.setHasFixedSize(true)
        rv.setItemViewCacheSize(20)
        rv.itemAnimator = DefaultItemAnimator()
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = packagesAdapter

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.mediator.observe(viewLifecycleOwner, Observer {  })
        viewModel.packages.observe(viewLifecycleOwner, Observer {
            packagesAdapter.setPackages(it)
        })

        viewModel.startNavigationEvent.observe(viewLifecycleOwner, Observer {
            val intent = Intent(requireContext(), GpsMapActivity::class.java)
            intent.putExtra(INTENT_ACTIVITY_KEY, TRACE_PACKAGES_FRAGMENT)
            startActivity(intent)
        })

        packagesAdapter.packageActionEvent.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandledOrReturnNull()?.let {
                newState -> viewModel.updatePackageState(newState)
            }
        })
    }
}