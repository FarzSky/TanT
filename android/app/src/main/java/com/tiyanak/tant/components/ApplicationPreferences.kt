package com.tiyanak.tant.components

import android.content.Context
import com.tiyanak.tant.R
import com.tiyanak.tant.data.file_system.EncryptedPreferences

class ApplicationPreferences private constructor(context: Context) {

    private val preferences: EncryptedPreferences

    private val APP_PACKAGE: String
    private val PREFERENCES_FILE: String

    companion object {
        private var instance: ApplicationPreferences? = null

        fun getInstance(context: Context): ApplicationPreferences {
            if (instance == null) {
                instance =
                    ApplicationPreferences(context)
            }
            return instance as ApplicationPreferences
        }
    }

    init {
        APP_PACKAGE = context.getString(R.string.app_package)
        PREFERENCES_FILE = APP_PACKAGE + "_preferences"
        preferences = EncryptedPreferences(context)
    }

    fun getString(key: String): String? {
        return preferences.getData(key)?.toString(Charsets.UTF_8)
    }

    fun setString(key: String, value: String) {
        preferences.setData(key, value.toByteArray())
    }

    fun clear(key: String) {
        preferences.remove(key)
    }
}