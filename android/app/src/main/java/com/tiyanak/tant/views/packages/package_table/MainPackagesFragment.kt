package com.tiyanak.tant.views.packages.package_table

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.tiyanak.tant.R
import com.tiyanak.tant.views.packages.create_package.CreatePackageActivity
import kotlinx.android.synthetic.main.fragment_main_packages.*

class MainPackagesFragment : Fragment() {

    companion object {
        fun newInstance() = MainPackagesFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_packages, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setAdapter()
        setFab()
    }

    private fun setAdapter() {
        val sectionsPagerAdapter = SectionsPagerAdapter(
            requireContext(), childFragmentManager
        )

        view_pager_packages.adapter = sectionsPagerAdapter
        tabs_packages.setupWithViewPager(view_pager_packages)
    }

    private fun setFab() {
        fab_create_package.setOnClickListener { view ->
            startActivity(Intent(requireContext(), CreatePackageActivity::class.java))
        }
    }
}