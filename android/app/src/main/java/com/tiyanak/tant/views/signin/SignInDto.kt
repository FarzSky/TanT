package com.tiyanak.tant.views.signin

data class SignInDto(
    val username: String = "",
    val password: String = "",
    val usernameError: Int? = null,
    val passwordError: Int? = null,
    val isDataValid: Boolean = false
)