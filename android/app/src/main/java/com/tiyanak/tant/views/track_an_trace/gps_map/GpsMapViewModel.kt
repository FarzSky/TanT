package com.tiyanak.tant.views.track_an_trace.gps_map

import androidx.lifecycle.*
import com.tiyanak.tant.common.Event
import com.tiyanak.tant.common.constants.DELIVERED
import com.tiyanak.tant.common.constants.IN_STORAGE
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC
import com.tiyanak.tant.views.packages.package_table.dto.PackageUpdateStateDto
import com.tiyanak.tant.views.track_an_trace.dto.DeliveryDto
import java.util.*

class GpsMapViewModel(val packageUC: IPackageUC) : ViewModel() {

    private val _deliveries: MutableLiveData<List<DeliveryDto>> = MutableLiveData()
    val deliveries: LiveData<List<DeliveryDto>> = _deliveries
    val deliveriesMediator = MediatorLiveData<Unit>()

    val deliveriesWithCoords = MutableLiveData<List<DeliveryDto>>()

    var currentPackageIndex: MutableLiveData<Int> = MutableLiveData(0)

    val navigationEvent: MutableLiveData<Event<Int?>> = MutableLiveData()

    init {
        deliveriesMediator.addSource(
            Transformations.switchMap(packageUC.getInDeliveryPackages()) {
                MutableLiveData(
                    it.map { p ->
                        DeliveryDto(
                            receiver = p.primatelj,
                            location = p.lokacija,
                            phone = p.telefon,
                            post = p.posta,
                            city = p.grad,
                            address = p.adresa,
                            `package` = p.paket,
                            price = p.cijena.toDouble(),
                            smsSent = p.smsSent
                        )
                    }
                )
            }
        ) {
            _deliveries.value = it.toMutableList()
        }
    }

    fun onDeliveryConfirmed() {
        val confirmedPackage = currentPackageIndex.value?.let { _deliveries.value?.get(it) }
        confirmedPackage?.let {
            packageUC.updatePackageState(
                PackageUpdateStateDto(
                    receiver = it.receiver,
                    phone = it.phone,
                    city = it.city,
                    address = it.address,
                    post = it.post,
                    `package` = it.`package`,
                    price = it.price,
                    state = DELIVERED,
                    deliveredAt = Date()
                )
            )
        }
    }

    fun onCancelDelivery() {
        val canceledPackage = currentPackageIndex.value?.let { _deliveries.value?.get(it) }
        canceledPackage?.let {
            packageUC.updatePackageState(
                PackageUpdateStateDto(
                    receiver = it.receiver,
                    phone = it.phone,
                    city = it.city,
                    address = it.address,
                    post = it.post,
                    `package` = it.`package`,
                    price = it.price,
                    state = IN_STORAGE,
                    deliveredAt = Date()
                )
            )
        }
    }

    fun navigateAll() {
        navigationEvent.postValue(Event(null))
    }

    fun navigateCurrent() {
        navigationEvent.postValue(Event(currentPackageIndex.value))
    }

    fun previousPackage() {
        val currentIndex = currentPackageIndex.value ?: 0
        if (currentIndex <= 0) {
            return
        }

        currentPackageIndex.postValue(currentPackageIndex.value?.dec())
    }

    fun nextPackage() {
        val currentIndex = currentPackageIndex.value ?: 0
        if (currentIndex + 1 >= (_deliveries.value?.size ?: 0)) {
            return
        }

        currentPackageIndex.postValue(currentPackageIndex.value?.inc())
    }
}