package com.tiyanak.tant.data.db.dao.interfaces

import androidx.lifecycle.LiveData
import androidx.room.*
import com.tiyanak.tant.common.constants.DELIVERED
import com.tiyanak.tant.data.db.entities.Package
import java.util.*

@Dao
interface IPackageDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun createPackage(packageEntity: Package)

    @Query("SELECT * FROM Package as p WHERE p.state = :packageState")
    fun getPackagesByStatus(packageState: String): LiveData<List<Package>>

    @Query("UPDATE Package SET state=:state WHERE primatelj = :receiver AND posta = :post AND adresa = :address AND paket = :packageItem")
    suspend fun updatePackageState(receiver: String, post: Int, address: String, packageItem: String, state: String)

    @Query("DELETE FROM Package WHERE primatelj = :receiver AND posta = :post AND adresa = :address AND paket = :packageItem")
    suspend fun deletePackage(receiver: String, post: Int, address: String, packageItem: String)

    @Query("SELECT * FROM Package as p WHERE date(p.deliveredAt) >= date(:from) AND date(p.deliveredAt) <= date(:to) AND p.state = :state")
    fun getByDateDeliveredAndStatus(from: Date, to: Date, state: String): LiveData<List<Package>>

    @Query("SELECT count(*) FROM Package as p WHERE p.state = '$DELIVERED' GROUP BY p.deliveredAt")
    fun countPerDay(): LiveData<List<Int>>

    @Query("SELECT p.grad as city, count(*) as visits FROM Package as p WHERE p.state = '$DELIVERED' GROUP BY p.grad")
    fun getFavouriteLocation(): LiveData<List<PackageFavouriteLocation>>

    @Query("SELECT EXISTS(SELECT * FROM Package WHERE primatelj = :receiver AND posta = :post AND adresa = :address AND paket = :packageItem)")
    fun exists(receiver: String, post: Int, address: String, packageItem: String): Boolean

    @Query("UPDATE Package SET smsSent=1 WHERE primatelj = :receiver AND posta = :post AND adresa = :address AND paket = :packageItem")
    suspend fun setSmsSent(receiver: String, post: Int, address: String, packageItem: String)

    @Query("SELECT * FROM Package")
    fun getAll(): List<Package>
}

data class PackageFavouriteLocation(
    val city: String,
    val visits: Int
)