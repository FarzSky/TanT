package com.tiyanak.tant.views.packages.package_table.packages_history

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tiyanak.tant.core.usecases.UseCaseFactory

class PackagesHistoryViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PackagesHistoryViewModel::class.java)) {
            return PackagesHistoryViewModel(UseCaseFactory.createPackageUC()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}