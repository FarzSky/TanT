package com.tiyanak.tant.async

import android.content.Context
import android.location.Address
import android.location.Geocoder
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


class AsyncGeocoder(context: Context?) {

    private val geocoder: Geocoder

    init {
        geocoder = Geocoder(context)
    }

    fun reverseGeocode(
        location: String,
        callback: Callback
    ): Disposable {
        return Observable.fromCallable {
            try {
                return@fromCallable geocoder.getFromLocationName(location, 1)
            } catch (e: Exception) {
                Timber.d("throwable,", Gson().toJson(e))
                e.printStackTrace()
            }
            false
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                //Use result for something
                Timber.d("throwable,", Gson().toJson(result))
                callback.success((result as List<*>)[0] as Address)
            }, { throwable -> Timber.d("throwable,", Gson().toJson(throwable)) })
    }

    interface Callback {
        fun success(address: Address?)
        fun failure(e: Throwable?)
    }
}