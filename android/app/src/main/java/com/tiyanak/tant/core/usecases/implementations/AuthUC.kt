package com.tiyanak.tant.core.usecases.implementations

import com.tiyanak.tant.App
import com.tiyanak.tant.components.ApplicationPreferences
import com.tiyanak.tant.data.db.entities.SignedInUser
import com.tiyanak.tant.core.usecases.interfaces.IAuthUC
import com.tiyanak.tant.data.Result
import com.tiyanak.tant.data.db.dao.interfaces.IAuthDao

class AuthUC(val dao: IAuthDao) : IAuthUC {
    // in-memory cache of the loggedInUser object
    var user: SignedInUser? = null
        private set

    val _isSignedIn: Boolean
        get() = user != null || isUserSignedIn()

    init {
        user = null
    }

    companion object {
        const val CURRENT_USER_PREF_KEY = "current_user"
    }

    override fun isSignedIn(): Boolean {
        return _isSignedIn
    }

    override fun signOut() {
        user = null
        dao.signout()
        signOutCurrentUser()
    }

    override fun signIn(username: String, password: String): Result<SignedInUser> {
        val result = dao.signIn(username, password)

        if (result is Result.Success) {
            setSignedInUser(result.data, password)
        }

        return result
    }

    override fun getUserName(): String {
        val preferences: ApplicationPreferences = ApplicationPreferences.getInstance(App.instance.applicationContext)
        return preferences.getString(CURRENT_USER_PREF_KEY)!!
    }

    private fun setSignedInUser(signedInUser: SignedInUser, password: String) {
        this.user = signedInUser
        saveCredentials(signedInUser.username, password)
    }

    // save user credentials for SSO
    private fun saveCredentials(username: String, password: String) {
        val preferences: ApplicationPreferences = ApplicationPreferences.getInstance(App.instance.applicationContext)
        preferences.setString(username, password)
        preferences.setString(CURRENT_USER_PREF_KEY, username)
    }

    private fun signOutCurrentUser() {
        val preferences: ApplicationPreferences = ApplicationPreferences.getInstance(App.instance.applicationContext)
        preferences.clear(CURRENT_USER_PREF_KEY)
    }

    private fun isUserSignedIn(): Boolean {
        val preferences: ApplicationPreferences = ApplicationPreferences.getInstance(App.instance.applicationContext)
        return preferences.getString(CURRENT_USER_PREF_KEY) != null
    }
}