package com.tiyanak.tant.core.usecases

import com.tiyanak.tant.App
import com.tiyanak.tant.core.usecases.implementations.AuthUC
import com.tiyanak.tant.core.usecases.implementations.PackageUC
import com.tiyanak.tant.core.usecases.interfaces.IAuthUC
import com.tiyanak.tant.core.usecases.interfaces.IPackageUC
import com.tiyanak.tant.data.db.AppDatabase
import com.tiyanak.tant.data.db.dao.implementations.AuthDao

class UseCaseFactory {

    companion object {
        fun createAuthUC() : IAuthUC {
            return AuthUC(AuthDao())
        }

        fun createPackageUC(): IPackageUC {
            return PackageUC(
                AppDatabase.getDatabase(App.instance).packageDao(),
                createAuthUC()
            )
        }
    }

}
