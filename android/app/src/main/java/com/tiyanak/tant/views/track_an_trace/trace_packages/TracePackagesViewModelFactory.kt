package com.tiyanak.tant.views.track_an_trace.trace_packages

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tiyanak.tant.core.usecases.UseCaseFactory

class TracePackagesViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TracePackagesViewModel::class.java)) {
            return TracePackagesViewModel(UseCaseFactory.createPackageUC()) as T
        }
        throw IllegalArgumentException("Unknown View Model")
    }

}