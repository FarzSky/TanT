package com.tiyanak.tant.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.tiyanak.tant.common.converters.DateConverter
import com.tiyanak.tant.data.db.entities.Package
import com.tiyanak.tant.data.db.dao.interfaces.IPackageDao
import java.util.concurrent.Executors

@Database(entities = [Package::class],
    version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun packageDao(): IPackageDao

    companion object {

        const val DATABASE_NAME: String = "track_an_trace_database"
        @Volatile private var INSTANCE: AppDatabase? = null
        private const val NUMBER_OF_THREADS = 4

        val databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS)

        fun getDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            AppDatabase::class.java, DATABASE_NAME
                        )
                        .build()
                    }
                }
            }
            return INSTANCE!!
        }

    }

}