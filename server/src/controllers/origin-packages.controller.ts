import express from "express";

import { PackageError } from './../models/types';
import DbProvider from './../db/dbProvider';
import PackagesXlsxDao from './../db/dao/xlsx/packagesXlsxDao';

class OriginPackagesController {
    public path = '/packages/origin';
    public router = express.Router();
    private packagesDao: PackagesXlsxDao;
 
    constructor() {
        this.intializeDb();
        this.intializeRoutes();
    }

    public intializeDb() {
        this.packagesDao = new PackagesXlsxDao(DbProvider.buildOriginPackagesXLSXDb());
    }

    public intializeRoutes() {
        this.router.get(this.path, this.getPackages);
    }

    /**
    * GET /packages
    * Gets all packages from the database
    */
    getPackages = async (request: express.Request, response: express.Response) =>  {
        try {
            const items = await this.packagesDao.getAll();
            response.status(200).send(items);
        } catch (error) {
            response.status(500).send(
                new PackageError("Error when fetching packages", error)
            );
        }
    }

}

export default OriginPackagesController;
