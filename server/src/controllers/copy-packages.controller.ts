import express from 'express';

import { PackageError } from './../models/types';
import DbProvider from './../db/dbProvider';
import PackagesCsvDao from './../db/dao/csv/packagesCsvDao';

class CopyPackagesController {
    public path = '/packages/copy';
    public router = express.Router();
    private packagesDao: PackagesCsvDao;
 
    constructor() {
        this.intializeDb();
        this.intializeRoutes();
    }

    public intializeDb() {
        DbProvider.buildCopyPackagesCSVDb().then(db => this.packagesDao = new PackagesCsvDao(db));
    }

    public intializeRoutes() {
        this.router.get(this.path, this.getPackages);
        this.router.post(this.path, this.createPackage);
        this.router.put(this.path, this.updatePackage);
        this.router.post(this.path + '/delete', this.deletePackage);
    }

    /**
     * GET /packages
     * Gets all packages from the database
     */
    getPackages = async (request: express.Request, response: express.Response) =>  {
        try {
            const items = await this.packagesDao.getAll();
            response.status(200).send(items);
        } catch (error) {
            response.status(500).send(
                new PackageError("Error when fetching packages", error)
            );
        }
    }

    /**
     * Creates a new package in db
     */
    createPackage = async (request: express.Request, response: express.Response) => {
        try {
            const itemToCreate = { ...request.body };
            await this.packagesDao.create(itemToCreate);
            response.status(201).send();
        } catch (error) {
            response.status(500).send(
                new PackageError("Error when creating package", error)
            );
        }
    }

    /**
     * Update old package with new values
     */
    updatePackage = async (request: express.Request, response: express.Response) => {
        try {
            const itemToUpdate = { ...request.body };
            await this.packagesDao.update(itemToUpdate.oldPackage, itemToUpdate.newPackage);
            response.status(201).send();
        } catch (error) {
            response.status(500).send(
                new PackageError("Error when creating package", error)
            );
        }
    }

    /**
     * Delete package
     */
    deletePackage = async (request: express.Request, response: express.Response) => { 
        try {
            const itemToUpdate = { ...request.body };
            await this.packagesDao.delete(itemToUpdate);
            response.status(201).send();
        } catch (error) {
            response.status(500).send(
                new PackageError("Error when creating package", error)
            );
        }
    }

}

export default CopyPackagesController;
