import { dbConfig } from "./../config/dbConfig";
import CsvDatabase from "./csvDatabase";
import XlsxDatabase from "./xlsxDatabase";

const csvdb = require("csv-database");

class DbProvider {

    static buildOriginPackagesCSVDb(): Promise<CsvDatabase> {
        return new Promise((resolve, reject) => {
            csvdb(dbConfig.originPackagesDbFile, dbConfig.packagesColumns, ';')
                .then(db => resolve(new CsvDatabase(db)))
                .catch(error => reject(error));
        });
    }

    static buildCopyPackagesCSVDb(): Promise<CsvDatabase> {
        return new Promise((resolve, reject) => {
            csvdb(dbConfig.copyPackagesDbFile, dbConfig.packagesColumns, ';')
                .then(db => resolve(new CsvDatabase(db)))
                .catch(error => reject(error));
        });
    }

    static buildOriginPackagesXLSXDb(): XlsxDatabase {
        return new XlsxDatabase(dbConfig.originPackagesDbFile);
    }

    static buildCopyPackagesXLSXDb(): XlsxDatabase {
        return new XlsxDatabase(dbConfig.copyPackagesDbFile);
    }
}

export default DbProvider;

