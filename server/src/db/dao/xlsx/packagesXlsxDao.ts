import Package from "src/models/entities/Package";
import XlsxDatabase from "src/db/xlsxDatabase";
import { Filter } from "./../../../models/filter";

class PackagesXlsxDao {

    private _db: XlsxDatabase;

    constructor(db: XlsxDatabase) {
        this._db = db;
    }

    public async getAll(): Promise<Package[]> {
        return await this._db.select([]);
    }

    public async create(newPackage: Package): Promise<void> {
        await this._db.insert(newPackage);
    }

    public async update(oldPackage: Package, newPackage: Package): Promise<void> {
        const conditions: Filter[] = [];
        Object.keys(oldPackage).forEach(key => {
            conditions.push(new Filter(key, '=', oldPackage[key]));
        });
        await this._db.update(newPackage, conditions);
    }
}

export default PackagesXlsxDao;