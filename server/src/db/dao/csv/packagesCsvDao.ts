import CsvDatabase from "../../csvDatabase";
import Package from "src/models/entities/Package";

class PackagesCsvDao {

    private _db: any;

    constructor(db: CsvDatabase) {
        this._db = db.getDb();
    }

    public async getAll(): Promise<Package[]> {
        return await this._db.get();
    }

    public async create(newPackage: Package): Promise<void> {
        await this._db.add(newPackage);
    }

    public async update(oldPackage: Package, newPackage: Package): Promise<void> {
        await this._db.edit({
            primatelj: oldPackage.primatelj, adresa: oldPackage.adresa, posta: oldPackage.posta, paket: oldPackage.paket
        }, newPackage);
    }

    public async delete(packageItem: Package): Promise<void> {
        await this._db.delete({
            primatelj: packageItem.primatelj, adresa: packageItem.adresa, posta: packageItem.posta, paket: packageItem.paket
        });
    }
}

export default PackagesCsvDao;