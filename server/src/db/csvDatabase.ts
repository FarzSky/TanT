class Database {
    
    private _db: any;
    
    constructor(db: any) {
        if (typeof db === 'undefined') {
            throw new Error('Cannot be called directly');
        }
        this._db = db
    }

    public getDb(): any {
        return this._db;
    }
}

export default Database;