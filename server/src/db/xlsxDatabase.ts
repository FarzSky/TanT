import { Filter } from "src/models/filter";

const xlsxDatabase = require('database-js-xlsx');

interface ConnectionConfig {
    Database: string;
}

class XlsxDatabase {
    
    private _file: string;
    private _connection: any;
    
    constructor(file: string) {
        if (typeof file === 'undefined') {
            throw new Error('Cannot be called directly');
        }
        this._file = file
    }

    public openConnection(): void {
        const opts: ConnectionConfig = { Database: this._file };
        this._connection = xlsxDatabase.open(opts);
    }

    public async closeConnection(): Promise<void> {
        if (this._connection)
            await this._connection.close();
    }

    public async select(conditions: Filter[]): Promise<any[]> {
        let result = [];
        try {
            let where = this.buildWhere(conditions);

            const query = `SELECT * FROM Sheet1 ${where}`;
            console.log('query', query);

            this.openConnection();
            result = await this._connection.query(query);
        } catch (error) {
            console.log(error);
        } finally {
            await this.closeConnection();
        }

        return result;
    }

    public async insert(row: any): Promise<any[]> {
        let result = [];
        try {
            const columns = [], values = [];
            Object.keys(row).forEach(k => {
                columns.push(`${k}`);
                values.push(`\`${row[k]}\``);
            });

            const query = `INSERT INTO Sheet1 (${columns.join(', ')}) VALUES (${values.join(', ')})`;
            console.log('query', query);

            this.openConnection();
            result = await this._connection.query(query);
        } catch (error) {
            console.log(error);
        } finally {
            await this.closeConnection();
        }

        return result;
    }

    public async update(row: any, conditions: Filter[]): Promise<any[]> {
        let result = [];
        try {
            const set = Object.keys(row).map(k => `${k}=\`${row[k]}\``).join(', ');

            const query = `UPDATE Sheet1 SET ${set} ${this.buildWhere(conditions)}`;
            console.log('query', query);
            
            this.openConnection();
            result = await this._connection.query(query);
        } catch (error) {
            console.log(error);
        } finally {
            await this.closeConnection();
        }

        return result;
    }

    public async delete(conditions: Filter[]): Promise<void> {
        try {
            const query = `DELETE FROM Sheet1 ${this.buildWhere(conditions)}`;
            console.log('query', query);
            
            this.openConnection();
            await this._connection.query(query);
        } catch (error) {
            console.log(error);
        } finally {
            await this.closeConnection();
        }
    }

    private buildWhere(conditions: Filter[]): String {
        if (!conditions || conditions.length === 0) return '';
        return `WHERE ${conditions.map(m => m.getCondition()).join(' AND ')}`;
    }
}

export default XlsxDatabase;