export const dbConfig = {
    originPackagesDbFile: './db/original.xlsx',
    copyPackagesDbFile: './db/copy.csv',
    packagesColumns: ['primatelj', 'adresa', 'posta', 'grad', 'telefon', 'paket', 'cijena']
};