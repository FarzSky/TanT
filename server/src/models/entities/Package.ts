import {
    Entity
} from "typeorm";

@Entity()
class Package {
    primatelj: string;
    adresa: string;
    posta: number;
    grad: string;
    telefon: string;
    paket: string;
    cijena: number;
}

export default Package;