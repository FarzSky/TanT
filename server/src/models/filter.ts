export class Filter {
    field: string;
    operation: string;
    value: any;

    constructor(filed: string, operation: string, value: any) {
        this.field = filed
        this.operation = operation;
        this.value = value;
    }

    public getCondition() {
        return `${this.field} ${this.operation} \`${this.value}\``;
    }
}