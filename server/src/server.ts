import App from './app';
import OriginController from './controllers/origin-packages.controller';
import CopyController from './controllers/copy-packages.controller';
 
const app = new App(
  [
    new OriginController(),
    new CopyController()
  ],
  8000,
);
 
app.listen();